<?php include("sanitizing_urls.php"); ?> 
 
 <aside class="main-sidebar" style="-webkit-box-shadow: 1px -1px 10px 0px rgba(0,0,0,0.94);background-color: #2ec0e4;
-moz-box-shadow: 1px -1px 10px 0px rgba(0,0,0,0.94);
box-shadow: 1px -1px 10px 0px rgba(0,0,0,0.94);">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" style="background-color: #2ec0e4">
        <div class="pull-left image">
          <center>
		        <a href="dashboard" class="login-logo"><img src="dist/img/logo_white.png" width="100%" ></a>

           </center>
        </div>
        <div class="pull-left info">
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li <?php if(isset($msg100_1))echo $msg100_1; ?>><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
       
        
       <li class="treeview <?php echo $msg21; ?>">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Task Management</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if(isset($msg21_1)) echo $msg21_1; ?>><a href="add_new_item"><i class="fa fa-edit"></i> Add New Task</a></li>
            <li <?php if(isset($msg21_2)) echo $msg21_2; ?>><a href="view_all_item"><i class="fa fa-cubes"></i> View All Tasks</a></li>
          </ul>
        </li>
        
         <li class="treeview <?php if(isset($msg700)) echo $msg700; ?>">
          <a href="#">
            <i class="fa fa-user"></i> <span>Members</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if(isset($msg700_2)) echo $msg700_2; ?>><a href="view_all_customer"><i class="fa fa-users"></i> View All Members </a></li>
            <li <?php if(isset($msg700_3)) echo $msg700_3; ?>><a href="blocked_customer"><i class="fa fa-ban"></i> Blocked Members </a></li>
           </ul>
        </li>
        
          <li class="treeview <?php echo $msg9902; ?>">
          <a href="#">
            <i class="fa fa-paper-plane"></i> <span>Payout Management </span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
	         <li <?php if(isset($msg9902_2)) echo $msg9902_2; ?>><a href="all_payout_rceived"><i class="fa fa-th-list"></i> Payout Received</a></li>
	         <li <?php  if(isset($msg9902_3)) echo $msg9902_3; ?>><a href="set_payout_rate"><i class="fa fa-th-list"></i> Payout Minimum Rate</a></li>
	         <li <?php if(isset($msg9902_4 )) echo $msg9902_4; ?>><a href="blocked_payout"><i class="fa fa-th-list"></i> Blocked Payout</a></li>
           </ul>
        </li>
        
        
        
          <li class="treeview <?php echo $msg9802; ?>">
          <a href="#">
            <i class="fa fa-money"></i> <span>Income History </span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
	         <li <?php if(isset($msg9802_2 )) echo $msg9802_2; ?>><a href="all_income_details"><i class="fa fa-th-list"></i> Income History</a></li>
	         <li <?php if(isset($msg9892_3 )) echo $msg9802_3; ?>><a href="income_details_by_date"><i class="fa fa-th-list"></i> Income History By Date</a></li>
	         <li <?php if(isset($msg9802_4 )) echo $msg9802_4; ?>><a href="income_history_by_group"><i class="fa fa-th-list"></i> Income History  Group</a></li>
             
	         <li <?php if(isset($msg9802_7 )) echo $msg9802_7; ?>><a href="main_income_history"><i class="fa fa-th-list"></i> Task income History </a></li>

	         <li <?php if(isset($msg9802_6 )) echo $msg9802_6; ?>><a href="down_income_history"><i class="fa fa-th-list"></i> Down income History </a></li>

	         <li <?php if(isset($msg9802_5 )) echo $msg9802_5; ?>><a href="member_down_self_in"><i class="fa fa-th-list"></i> Member down and self income</a></li>

           </ul>
        </li>
        
        
        
         <li class="treeview <?php echo $msg800; ?>">
          <a href="#">
            <i class="fa fa-line-chart"></i> <span>Reports</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php  if(isset($msg800_1 )) echo $msg800_1; ?>><a href="sales_statement"><i class="fa fa-area-chart"></i> Received Statement</a></li>
            
          </ul>
        </li>
        


        <li class="treeview <?php echo $msg80; ?>">
          <a href="#">
            <i class="fa fa-google"></i> <span>Google Ad Code </span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
             <li><a href="app_code.php"><i class="fa fa-edit"></i>Set App Code</a></li>
            </ul>
        </li>


        
        <li class="treeview <?php echo $msg80; ?>">
          <a href="#">
            <i class="fa fa-cogs"></i> <span>Change Setting</span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
             <li <?php if(isset($msg80_3 )) echo $msg80_3; ?>><a href="social_networks"><i class="fa fa-edit"></i>Social Networks</a></li>
            </ul>
        </li>

      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>