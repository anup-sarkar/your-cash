
<?php 
	$getID = $_SESSION['user_id'];
    if ($stmt = $mysqli->prepare("SELECT id, username, email
        FROM members
       WHERE id = ?
        LIMIT 1")) {
        $stmt->bind_param('s', $getID);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $username, $email);
        $stmt->fetch();
		}

?> 

<header class="main-header" style="z-index: 30">
    <!-- Logo -->
    <a href="dashboard" class="logo" style="background-color: #2ec0e4">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>></b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><br/> </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top navbar-light bg-light" style="-webkit-box-shadow: 2px -2px 10px 0px rgba(0,0,0,0.75);
-moz-box-shadow: 2px -2px 10px 0px rgba(0,0,0,0.75);
box-shadow: 2px -2px 10px 0px rgba(0,0,0,0.75);
background-color: #ffffff;
">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button" style="color:black">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="color:black">
              <img src="dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $email; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/avatar5.png" class="img-circle" alt="User Image">

                <p>
                  Administrator
                  <small><span class="hidden-xs"><?php echo $email; ?></span>
</small>
                </p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="edit_profile" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="includes/logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        
        </ul>
      </div>
    </nav>
  </header>
  
  <style type="text/css">
    
    .sidebar-menu>li
    {
      font-size: 16px
    }
  </style>