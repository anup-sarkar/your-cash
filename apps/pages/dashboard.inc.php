
<title>Admin Area | CMS</title>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
       
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
            <?php 		
			global $mysqli;
				if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
					$stmt_m->execute();    // Execute the prepared query.
						// get variables from result.
					$stmt_m->store_result();
					$stmt_m->bind_result($mmbrid);
					$stmt_m->fetch();
					$stmt_m->num_rows();
					$numrows = $stmt_m->num_rows;
						  }
				?>
              <h3><?php echo $numrows; ?></h3>
              <p>Total Members</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-contacts"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>




<div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
            <?php     
      global $mysqli;
        if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
          $stmt_m->execute();    // Execute the prepared query.
            // get variables from result.
          $stmt_m->store_result();
          $stmt_m->bind_result($mmbrid);
          $stmt_m->fetch();
          $stmt_m->num_rows();
          $numrows = $stmt_m->num_rows;
              }
        ?>
              <h3>100000</h3>
              <p>Total Payout Received</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>





<div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
            <?php     
      global $mysqli;
        if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
          $stmt_m->execute();    // Execute the prepared query.
            // get variables from result.
          $stmt_m->store_result();
          $stmt_m->bind_result($mmbrid);
          $stmt_m->fetch();
          $stmt_m->num_rows();
          $numrows = $stmt_m->num_rows;
              }
        ?>
              <h3>31</h3>
              <p>Total Tasks</p>
            </div>
            <div class="icon">
              <i class="fa fa-th"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>





      </div>
      <!-- /.row -->
       
       
                          
                          
                          
                                
       </section>




           <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
       
       
        <div class="col-lg-4 col-xs-6" >
          <!-- small box -->
          <div class="small-box  " style="border: 1px solid #e5e5e5">
            <div class="inner">
            <?php     
      global $mysqli;
        if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
          $stmt_m->execute();    // Execute the prepared query.
            // get variables from result.
          $stmt_m->store_result();
          $stmt_m->bind_result($mmbrid);
          $stmt_m->fetch();
          $stmt_m->num_rows();
          $numrows = $stmt_m->num_rows;
              }
        ?>
              <h3>10000</h3>
              <p>Total Income This Month</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>




<div class="col-lg-4 col-xs-6">
          <!-- small box -->
                  <div class="small-box  " style="border: 1px solid #e5e5e5">
            <div class="inner">
            <?php     
      global $mysqli;
        if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
          $stmt_m->execute();    // Execute the prepared query.
            // get variables from result.
          $stmt_m->store_result();
          $stmt_m->bind_result($mmbrid);
          $stmt_m->fetch();
          $stmt_m->num_rows();
          $numrows = $stmt_m->num_rows;
              }
        ?>
              <h3>10000</h3>
              <p>Total Payout This Month</p>
            </div>
            <div class="icon">
              <i class="fa fa-usd"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>





<div class="col-lg-4 col-xs-6">
          <!-- small box -->
                   <div class="small-box  " style="border: 1px solid #e5e5e5">
            <div class="inner">
            <?php     
      global $mysqli;
        if ($stmt_m = $mysqli->prepare("SELECT id FROM sd_client WHERE type = 1")){
          $stmt_m->execute();    // Execute the prepared query.
            // get variables from result.
          $stmt_m->store_result();
          $stmt_m->bind_result($mmbrid);
          $stmt_m->fetch();
          $stmt_m->num_rows();
          $numrows = $stmt_m->num_rows;
              }
        ?>
              <h3>100</h3>
              <p>Total App Download</p>
            </div>
            <div class="icon">
              <i class="fa fa-android"></i>
            </div>
            <a href="view_all_customer" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>





      </div>
      <!-- /.row -->
       
       
                          
                          
                          
                                
       </section>



    <!-- /.content -->
  </div>