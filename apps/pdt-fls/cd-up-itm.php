<?php
include_once '../functions/functions.php'; 
testing_loggin();

 	if (isset($_POST['item_name'] , $_POST['price'])) {
	
		// Sanitize and validate the data passed in
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);		 
		$title = filter_input(INPUT_POST, 'item_name', FILTER_SANITIZE_STRING);
 		$discount_price = filter_input(INPUT_POST, 'discount_price', FILTER_SANITIZE_STRING);
 		$date = filter_input(INPUT_POST, 'date', FILTER_SANITIZE_STRING);
 		$price = filter_input(INPUT_POST, 'price', FILTER_SANITIZE_STRING);
 		$activity = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);

 			$inter = $_POST['inter'];
 				$banner = $_POST['banner'];
 						$click = $_POST['click'];

 							$stepName = $_POST['stepName'];
 								$details = $_POST['stepDetails'];
 									$link = $_POST['link'];
 										$type = $_POST['type'];
 											$order = $_POST['order'];
		$last_up = date('Y-m-d');

		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_item_l SET item_name=?, price=?, generation=?, up_date=?, activity2=? ,interID=?,bannerID=?,clickID=?,StepTitle=?,StepDetails=?,StepLink=?,StepType=?,SortOrder=? WHERE id = ? LIMIT 1")){
		// Bind the variables:
		$insert_stmt->bind_param('sssssssssssssi', $title, $price,  $discount_price, $last_up, $activity,$inter,$banner,$click,$stepName,$details,$link,$type,$order, $id);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
       }
	   
	   
		  header('Location: ../../update_item/'.$id.'/success');
 		}
	
	
?>