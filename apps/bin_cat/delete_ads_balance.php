<?php
define("APP_ROOT", dirname(dirname(__FILE__)));
define("PRIVATE_PATH", APP_ROOT . "");
require_once(PRIVATE_PATH . "/functions/functions.php");
testing_loggin();
 
 	if (isset($_GET['catID'])) {
	
		// Sanitize and validate the data passed in
		$menu_id = filter_input(INPUT_GET, 'catID', FILTER_SANITIZE_STRING);
		$date = filter_input(INPUT_GET, 'date', FILTER_SANITIZE_STRING);
		$activity = '0';
		$last_up = date('Y-m-d');
		
        // Delete From database 
		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_point_count SET remove_time=?, activity=?  WHERE type = 2 AND dipo_to = ? AND date = ? ")){
		// Bind the variables:
		$insert_stmt->bind_param('ssss', $last_up, $activity, $menu_id, $date);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
		   
		    header('Location: ../../main_income_history/'.$date.'/success');
		}
	 
	 }
?>