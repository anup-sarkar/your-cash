<?php
define("APP_ROOT", dirname(dirname(__FILE__)));
define("PRIVATE_PATH", APP_ROOT . "");
require_once(PRIVATE_PATH . "/functions/functions.php");
testing_loggin();
 
 	if (isset($_GET['catID'])) {
	
		// Sanitize and validate the data passed in
		$menu_id = filter_input(INPUT_GET, 'catID', FILTER_SANITIZE_NUMBER_INT);
		$activity = '0';
		$last_up = date('Y-m-d');
		
        // Delete From database 
		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_point_count SET remove_time=?, activity=?  WHERE id = ? LIMIT 1")){
		// Bind the variables:
		$insert_stmt->bind_param('ssi', $last_up, $activity, $menu_id);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
		   
		    header('Location: ../../all_income_details/932541/success');
		}
	 
	 }
?>