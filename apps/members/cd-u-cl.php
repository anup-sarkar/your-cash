<?php
require_once("../functions/functions.php");
testing_loggin();
 	if (isset($_POST['name'])) {
	
		// Sanitize and validate the data passed in
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
		$name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
		$mobile = filter_input(INPUT_POST, 'mobile', FILTER_SANITIZE_STRING);
		$email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
		$password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
		$hash_pass = md5($password);
		$imei = filter_input(INPUT_POST, 'imei', FILTER_SANITIZE_STRING);
		$ref = filter_input(INPUT_POST, 'ref', FILTER_SANITIZE_STRING);
 		$last_up = date('Y-m-d');
		$activity = filter_input(INPUT_POST, 'activity', FILTER_SANITIZE_STRING);
		
		if ($stmt_m = $mysqli->prepare("SELECT id
    	    FROM sd_client
     			  WHERE mobile = ?
        ORDER BY id DESC LIMIT 1 ")) {
        $stmt_m->bind_param('s', $ref);  // Bind "$email" to parameter.
        $stmt_m->execute();    // Execute the prepared query.
        $stmt_m->store_result();
        // get variables from result.
        $stmt_m->bind_result($refID);
        $stmt_m->fetch();
		$stmt_m->close();
		}
		
		
		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_client SET  name=?, email=?, password=?, pass_hash=?, imei=?, ref=?, ref_id=?, activity=? WHERE id = ? LIMIT 1")){
		// Bind the variables:
		$insert_stmt->bind_param('ssssssssi',  $name, $email, $password, $hash_pass, $imei, $ref, $refID, $activity, $id);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
       }
	   
		  header('Location: ../../update_client/'.$id.'/success');
 		}
	
?>