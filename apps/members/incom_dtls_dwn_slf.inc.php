<?php require_once("apps/initialize.php"); 
$ItemID = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
$u_ItemID = urlencode($ItemID);

$url_link = isset($_GET['msgID']) ? $_GET['msgID'] : '';
$u_link = urlencode($url_link);

if (!empty($_POST['to'])){
	$date = filter_input(INPUT_POST, 'to', FILTER_SANITIZE_STRING);
	$customerID = filter_input(INPUT_POST, 'customerID', FILTER_SANITIZE_STRING);
	
		
}
else
{

	
	$date = $u_ItemID;
 	$customerID = $u_link;

}

 
?>


<!--Datepicker Start -->
  <link rel="stylesheet" href="dist/css/jquery-ui.css">
  <script src="js/jquery-1.10.2.js"></script>
  <script src="js/jquery-ui.js"></script>
<script>
  $(function() {
    $( "#datepicker" ).datepicker({
      altField: "#alternate",
      altFormat: "DD, d MM, yy"
    });
  });
  
  $(function() {
    $( "#datepicker2" ).datepicker({
      altField: "#alternate",
      altFormat: "DD, d MM, yy"
    });
  });
  </script>
 <!--Datepicker Close -->
 

  <title>View Down And Self Income Details</title>
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>View Down And Self Income Details</h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>    report</li>
        <li class="active">View Task report</li>
      </ol>
    </section>

<!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <p>
          
          <div class="row">
          
        <div class="col-xs-12">
        
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Search A customer  </h3>
            </div>
            <!-- /.box-header -->
    
                       
   <form action="" enctype="multipart/form-data"  method="POST">
            <div class="box-body" >
            
				<div class="col-md-4">  
					<div class="form-group">
						<label for="exampleInputEmail1">Search By customer email/Mobile</label>
						<select name="customerID" id="product" class='select2 form-control' style="border: 0px none; padding:0px;">
						 <option value="">Select A Customer Name </option>
							<?php global $mysqli;
								$country_id = urlencode('1');
								$stmt = $mysqli->prepare("SELECT id,mobile, name, email FROM sd_client 
								  ORDER BY id ASC");
								$stmt->execute();
								$stmt->bind_result($id, $customer_mobile, $name, $email);
								while ($stmt->fetch()) {
									echo "<option value='$customer_mobile'>$name, &nbsp; ($customer_mobile)</option>";
								}
								$stmt->close();
							
							?>
						</select>                        
					 </div>
                 </div>
                   
                   
				<div class="col-md-4"> 
                <strong>Date</strong>
				<div class="form-group ">			
                    <input type="text" name="to" class="form-control pull-right" autocomplete="off" id="datepicker2" value="<?php echo $date; ?>" placeholder="To">
		 		  </div>
			  </div>

			  
                <div class="col-md-1" style="margin-top: 14px;"> 
					<div class="box-footer button-demo" style="border: 0px none;">
            		  <button class="btn btn-success pull-right"><i class="fa fa-search"></i> Search...</button>
           		   </div>	 
 				</div>   
           
                
              </div>
              <!-- /.box-body -->
            </form>
          </div>
          
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Data List</h3>
              <div class="box-tools">                              
               
              </div>
            </div>
            <!-- /.box-header -->
            
        	<?php 		
								$url_link = isset($_GET['msgID']) ? $_GET['msgID'] : 'nothing_yet';
								$u_link = urlencode($url_link);
								if ($u_link == "success"){
								echo '
									<div class="alert alert-dismissable alert-danger" style="visibility: visible; opacity: 1; display: block; transform: translateY(0px);">
						<i class="fa fa-close"></i>&nbsp; <strong>Delete Successful!</strong>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					</div>
			';
			 		}
			  		else{}
								
								
 			?>
 
 
 						
	<table class="table table-hover">
		  <thead>
             <tr class="info_member">
 								<th width="8%">Date </th>
                                <th width="8%">Name</th>
                                <th width="8%">Mobile</th>
                                <th width="8%">Type</th>
                                  <th width="7%">Amount</th>
                           <?php  echo '<th width="7%" style="text-align: center;">Action</th>'; ?>
                           	</tr>
                        </thead>
                       <tbody>
					 <?php 		
 							 if ($stmt_m = $mysqli->prepare("SELECT  name, l_name 
						 		from sd_client
                                    WHERE mobile = ? ")){
                                $stmt_m->bind_param('s', $customerID);  // Bind "$email" to parameter.
                                $stmt_m->execute();    // Execute the prepared query.
                                // get variables from result.
                                $stmt_m->bind_result($m_name, $m_lname);
                                $stmt_m->store_result();
                                $stmt_m->fetch();
                                 $stmt_m->close();
                                  }	

								if ($stmt_m = $mysqli->prepare("SELECT SUM(amount) 
									from sd_point_count
										WHERE activity = 1 AND type = 1 AND date = '".$date."' AND dipo_to = ? ")){
									$stmt_m->bind_param('s', $customerID);  // Bind "$email" to parameter.
									$stmt_m->execute();    // Execute the prepared query.
									// get variables from result.
									$stmt_m->bind_result($ttl_amount);
									$stmt_m->store_result();
									$stmt_m->fetch();
									 $stmt_m->close();
									  }	
									  
								if ($stmt_m = $mysqli->prepare("SELECT SUM(amount) 
									from sd_point_count
										WHERE activity = 1 AND type = 2 AND date = '".$date."' AND dipo_to = ? ")){
									$stmt_m->bind_param('s', $customerID);  // Bind "$email" to parameter.
									$stmt_m->execute();    // Execute the prepared query.
									// get variables from result.
									$stmt_m->bind_result($ttl_self_incm);
									$stmt_m->store_result();
									$stmt_m->fetch();
									 $stmt_m->close();
									  }			  
									  	
							 
						 ?>               
                            <tr>
                            
 								 <td><?php echo $date;?></td>
                                <td><?php  echo $m_name . $m_lname;  ?></td>
                                <td><?php echo $customerID;?></td>
                                 <td>Total Down Income</td>
                                <td><?php if($ttl_amount == ''){echo 0;}else{ echo $ttl_amount;} ?></td>
                                  <td style="text-align: center;">
                                      <a href="apps/bin_cat/delete_dwn_slf_balance.php?actionID=view_all_item&date=<?php echo $date; ?>&catID=<?php echo $customerID; ?>&type=1" class="btn btn-danger btn-raised btn-xs" data-toggle="tooltip" data-placement="top" title="Delete This Row" onClick="return confirm('Are you sure to Delete this Item?')"><i class="fa fa-close"></i><div class="ripple-container"></div></a>
                                      
                                       </td>
                            </tr>
   						 	 
                               <tr>
                            
 								 <td><?php echo $date;?></td>
                                <td><?php  echo $m_name . $m_lname;  ?></td>
                                <td><?php echo $customerID;?></td>
                                 <td>Total Self Income</td>
                                <td><?php if($ttl_self_incm == ''){echo 0;}else{echo $ttl_self_incm;}   ?></td>
                                  <td style="text-align: center;">
                                      <a href="apps/bin_cat/delete_dwn_slf_balance.php?actionID=view_all_item&date=<?php echo $date; ?>&catID=<?php echo $customerID; ?>&type=2" class="btn btn-danger btn-raised btn-xs" data-toggle="tooltip" data-placement="top" title="Delete This Row" onClick="return confirm('Are you sure to Delete this Item?')"><i class="fa fa-close"></i><div class="ripple-container"></div></a>
                                      
                                       </td>
                            </tr>
   						 	 
                             
                             
                        </tbody>
                    </table>
                    
                              
          
                             <!-- /.box-body -->
                          </div>
                          <!-- /.box -->
      				  </div>
     			 </div>
           </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>  
<script src="dist/js/select2.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="dist/css/select2.css"/>
<link rel="stylesheet" type="text/css" href="dist/css/select2-bootstrap.css"/>
  <script>
      $('.select2').select2({ placeholder : '' });

      $('.select2-remote').select2({ data: [{id:'A', text:'A'}]});

      $('button[data-select2-open]').click(function(){
        $('#' + $(this).data('select2-open')).select2('open');
      });
  </script>  