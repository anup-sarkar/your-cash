<?php
include_once '../functions/functions.php'; 
testing_loggin();
 
 	if (strlen($_POST['id'])) {
	
		// Sanitize and validate the data passed in
		$menu_id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
		$inter = filter_input(INPUT_POST, 'inter', FILTER_SANITIZE_STRING);
		$banner = filter_input(INPUT_POST, 'banner', FILTER_SANITIZE_STRING);
		$click = filter_input(INPUT_POST, 'click', FILTER_SANITIZE_STRING);
		 

		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_app_codes SET interID=?, bannerID=?, clickID=?  WHERE id = ? LIMIT 1")){
		// Bind the variables:
		$insert_stmt->bind_param('ssss', $inter, $banner, $click, $menu_id);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Update failure: INSERT');
           }
       }
			  header('Location: ../../app_code.php?'.$menu_id.'=success');

 	}
 	else
 	{
 		// Sanitize and validate the data passed in
	 
		$inter = filter_input(INPUT_POST, 'inter', FILTER_SANITIZE_STRING);
		$banner = filter_input(INPUT_POST, 'banner', FILTER_SANITIZE_STRING);
		$click = filter_input(INPUT_POST, 'click', FILTER_SANITIZE_STRING);
		 

		// Prepare the statement:
		global $mysqli;
		 

               if ($insert_stmt = $mysqli->prepare("INSERT INTO sd_app_codes ( interID, bannerID, clickID) values ( ?,?,?)")) {
            $insert_stmt->bind_param('sss', $inter, $banner, $click);
            // Execute the prepared query.

            //echo $insert_stmt;


            if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
       }
	 


       			
			 header('Location: ../../app_code.php');
 	}
	
	
?>