<?php 
require_once("apps/initialize.php"); 
$sanitize = true;

class my_co_class {

 public function setPageUrl()
  	{
		$url_link = isset($_GET['actionID']) ? $_GET['actionID'] : 'nothing_yet';
		return $u_link = urlencode($url_link);
	}

 public function setPage($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/pages/".$this->headery."");
   }
  
   public function set_itms($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/pdt-fls/".$this->headery."");
   }
  public function set_sales_person_create($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/person_admin/".$this->headery."");
   }

  public function set_customer($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/members/".$this->headery."");
   }

  public function set_setting($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/ac_settings/".$this->headery."");
   }
   
  public function set_order($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/sales/".$this->headery."");
   }

    public function set_sting($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/niast/".$this->headery."");
   }
  
    public function set_statement($headery)
   {
	$this->headery = $headery;
	global $mysqli; 
	include_once(PRIVATE_PATH . "/statement/".$this->headery."");
   }
   
}

$obj = new my_co_class();

if ($obj->setPageUrl() == 'dashboard')
	{
		$obj->setPage("dashboard.inc.php");
	}
	 
// Item  Value
if ($obj->setPageUrl() == 'add_new_item')
	{
		$obj->set_itms("itm-ins-lsf.php");
	}
if ($obj->setPageUrl() == 'view_all_item')
	{
		$obj->set_itms("itm-all.inc.php");
	}
if ($obj->setPageUrl() == 'update_item')
	{
		$obj->set_itms("up-itm.inc.php");
	}

 

	 
// End Item  Value

if ($obj->setPageUrl() == 'view_all_customer')
	{
		$obj->set_customer("all_members.inc.php");
	}	


if ($obj->setPageUrl() == 'multi_level')
	{
		$obj->set_customer("multi_level.php");
	}	
	
	if ($obj->setPageUrl() == 'update_client')
	{
		$obj->set_customer("clnt-up-sngl.inc.php");
	}	
if ($obj->setPageUrl() == 'sales_statement')
	{
		$obj->set_statement("sales_statement.inc.php");
	}
 
 
 if ($obj->setPageUrl() == 'blocked_customer')
	{
		$obj->set_customer("blcked_all_mmbr.inc.php");
	}	

 if ($obj->setPageUrl() == 'all_income_details')
	{
		$obj->set_customer("incom_all_lst.inc.php");
	}	
	
 if ($obj->setPageUrl() == 'income_details_by_date')
	{
		$obj->set_customer("incom_dtls_by_dt.inc.php");
	}	

 if ($obj->setPageUrl() == 'income_history_by_group')
	{
		$obj->set_customer("incom_dtls_by_grp.inc.php");
	}	

 if ($obj->setPageUrl() == 'down_income_history')
	{
		$obj->set_customer("dwn_incm_hstory.inc.php");
	}	

  if ($obj->setPageUrl() == 'main_income_history')
	{
		$obj->set_customer("min_incm_dtls.inc.php");
	}	

 if ($obj->setPageUrl() == 'member_down_self_in')
	{
		$obj->set_customer("incom_dtls_dwn_slf.inc.php");
	}	


if ($obj->setPageUrl() == 'ref_bonus')
	{
		$obj->set_customer("all_bns_lst.inc.php");
	}	

if ($obj->setPageUrl() == 'set_payout_rate')
	{
		$obj->set_sting("sbscrib_pr.inc.php");
	}

if ($obj->setPageUrl() == 'blocked_payout')
	{
		$obj->set_sting("actv_blckd_pr.inc.php");
	}


// Settings  Value
 if ($obj->setPageUrl() == 'all_payout_rceived')
	{
		$obj->set_setting("trnsfr_all.inc.php");
	}

if ($obj->setPageUrl() == 'confirmed_payment_agent')
	{
		$obj->set_setting("cm-ap-agt.php");
	}


if ($obj->setPageUrl() == 'edit_home_step_one')
	{
		$obj->set_setting("step_one_edit.php");
	}
	
if ($obj->setPageUrl() == 'update_home_position')
	{
		$obj->set_setting("edit_home.inc.php");
	}
	
if ($obj->setPageUrl() == 'social_networks')
	{
		$obj->set_setting("scl-setng.inc.php");
	}
	
if ($obj->setPageUrl() == 'company_details')
	{
		$obj->set_setting("cmp-setng.inc.php");
	}
	
		
// End Settings  Value


// members  Value
if ($obj->setPageUrl() == 'add_new_person')
	{
		$obj->set_sales_person_create("new_sales_id.php");
	}

if ($obj->setPageUrl() == 'view_all_person')
	{
		$obj->set_sales_person_create("all_sales_id.php");
	}

	if ($obj->setPageUrl() == 'edit_profile')
	{
		$obj->set_customer("edit_profile.php");
	}	
	
if ($obj->setPageUrl() == 'change_company_details')
	{
		$obj->set_sting("update_company_info.inc.php");
	}
	
// End members  Value


unset($obj);

?>