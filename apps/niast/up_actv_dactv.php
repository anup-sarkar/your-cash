<?php
require_once("../functions/functions.php");
testing_loggin();

 	if (isset($_POST['company_name'], $_POST['id'])) {
	
		// Sanitize and validate the data passed in
		$id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT);
		$company_name = filter_input(INPUT_POST, 'company_name', FILTER_SANITIZE_STRING);
 		$last_up = filter_input(INPUT_POST, 'up_date', FILTER_SANITIZE_STRING);

		// Prepare the statement:
		global $mysqli;
		if ($insert_stmt = $mysqli->prepare("UPDATE sd_settings SET company=? WHERE id = ? LIMIT 1")){
		// Bind the variables:
		$insert_stmt->bind_param('si', $company_name, $id);
		// Execute the query:
    
             if (!$insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
           }
       }
	  header('Location: ../../blocked_payout/854223DF9/success');
 		}
	
	
?>