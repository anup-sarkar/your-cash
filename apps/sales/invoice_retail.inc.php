<?php ob_start();
include_once 'apps/functions/functions.php'; 
include_once("apps/sales/sales_stm.php");

$sanitize = true;
$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
$client_id = urlencode($cat_name);
						
global $mysqli;
if ($stmt_m = $mysqli->prepare("SELECT customerID, customer_name, mobile, extra_number, address, billing_address, order_id, discount,  date, activity, time, l_time FROM sd_order_more
			  WHERE order_id = ? ")){
$stmt_m->bind_param('s', $client_id);  // Bind "$email" to parameter.
$stmt_m->execute();    // Execute the prepared query.
		// get variables from result.
$stmt_m->store_result();
$stmt_m->bind_result($customerID, $customer_name, $customer_mobile, $extra_number, $customer_address, $billing_address, $orderID, $discount, $date, $activity, $time, $l_time);
$stmt_m->fetch();
$stmt_m->close();
	}
						  
$sql = "SELECT id, name, mobile, address, email FROM sd_client  WHERE  id = '".$customerID."'";
$member = mysqli_query($mysqli, $sql) or die("Opps some thing went wrong");
$row_member = mysqli_fetch_assoc($member); 
			 
?>
  <title>Invoice</title>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Invoice
        <small style="text-transform: uppercase;">#<?php echo $orderID; ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
  <div style="text-align:center"> 
                <?php $sales_mng_out->company_details(); ?>
               		 </div>
             <hr />
        <!-- /.col -->
      </div>
      
      <!-- info row -->
    <div class="row invoice-info">
      <div class="col-xs-12">
        <div class="col-sm-9 invoice-col">
        <?php 
         	 echo "Name: " . $customer_name , '<br>';
            echo "Mobile: " . $customer_mobile , '<br>';
            echo "Extra Number: " . $extra_number, '<br>';
            echo "Address: " .$customer_address, '<br>';
            echo "Billing Address: " .$billing_address;
	   
	  ?>
        </div>
        <!-- /.col -->
        <div class="col-sm-3 invoice-col" style="text-align: right;">
         <address>
           <strong> Invoice ID# <?php echo $client_id; ?></strong><br>
           Date:  <?php echo $date; ?> <br>
           Time:  	<?php echo $l_time; ?>			 <br>
          </address>
        </div>
        </div>
     </div>
      </div>
      <!-- /.row -->

 <center>
              <?php if($activity == 1){echo"<h2 style='color: #008347;'>Pending...</h2>";}if($activity == 2){echo"<h2 style='color: #0002BF;'>Order Completed...</h2>";}if($activity == 3){echo"canceled...";}?>
      	<?php if($activity == 1){ ?>
          <a href="approved/<?php echo $client_id; ?>" class="btn btn-danger" onclick="return confirm('Are you sure to Completed This Order?')" style="margin-left:15px; margin-bottom:10px;padding: 9px;font-size: 20px;">
        	<i class="fa fa-reply" aria-hidden="true" ></i>&nbsp; Order Completed</a>
            <?php } ?>
			</center>

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
                                <th style="width:3%;text-align: center;">SL</th>
                                <th style="width:8%;">Image</th>
                                <th style="width:15%;">Name</th>
                                <th style="width:7%;text-align: center;">Unit Price</th>
                                <th style="width:4%;text-align: center;">Qty</th>
                                <th style="text-align:right;width:10%;">Total</th>
            </tr>
            </thead>
            <tbody id="show">
                
                	<?php $i = 0; ?>
          			   <?php 		
                        global $mysqli;
                        if ($stmt = $mysqli->prepare("SELECT pro_id, title, item_code, img, price, qty, line_total from sd_order_list
						WHERE orderID = ? ")){
						$stmt->bind_param('s', $client_id);  // Bind "$email" to parameter.
                        $stmt->execute();    // Execute the prepared query.
                        // get variables from result.
				        $stmt->store_result();
                        $stmt->bind_result($pro_id, $title, $item_code, $img, $price, $qty, $line_total);
                          }
						  
						  while ($stmt->fetch()) { 

                         if ($stmt_m = $mysqli->prepare("SELECT  id, item_name, item_code from sd_item_l
                                    WHERE id = ? ")){
                                $stmt_m->bind_param('s', $pro_id);  // Bind "$email" to parameter.
                                $stmt_m->execute();    // Execute the prepared query.
                                // get variables from result.
                                $stmt_m->bind_result($pro_id, $item_name, $item_code);
                                $stmt_m->store_result();
                                $stmt_m->fetch();
                                $stmt_m->close();
                                  }	
								  
                    		  ?>   
                              <span  class="price" style="visibility:hidden; float:left; height:2px;"><?php echo $sell; ?></span>
                            <tr style="background-color: #fbfbfb;">
                               
                                <td style="text-align: center;"><?php echo ++$i;?></td>
                                 <td>  <img  src="../images/products/<?php echo $img; ?>" width="100" alt="<?php echo $img; ?>"></td>
                                <td><?php echo $title;?><br />
										Code: <?php echo $item_code; ?></td>
                                <td style="text-align: center;"><?php echo $price; ?></td>
                                <td style="text-align: center;"><?php echo $qty; ?></td>
                                <td style="text-align:right;"><?php echo $line_total; ?></td>
                            </tr>
   						 	 <?php }
							 		$stmt->close();
								?>   
          		 </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-sm-6">
          <p class="lead">&nbsp;</p>
</div>
        <!-- /.col -->
        <div class="col-sm-6">
				<?php 			
						$sql = "select sum(line_total) from sd_order_list  WHERE  orderID = '".$client_id."'";
						$sold = mysqli_query($mysqli, $sql) or die("Opps some thing went wrong");
						$row_in_total = mysqli_fetch_array($sold); 
				
						global $mysqli;
						if ($stmt_m = $mysqli->prepare("SELECT customerID, discount, shipping, mathod, g_total FROM sd_order_more
						  WHERE order_id = ? ")){
						$stmt_m->bind_param('s', $client_id);  // Bind "$email" to parameter.
						$stmt_m->execute();    // Execute the prepared query.
						// get variables from result.
						$stmt_m->store_result();
						$stmt_m->bind_result($customer_id,  $discount, $shipping, $mathod, $g_total);
						$stmt_m->fetch();
						$stmt_m->close();
						  }		
						?>
          <div class="table-responsive">
        
          
            <table class="table">
              <tr>
                <th align="right"><strong>Sub Total</strong></th>
                <td style="text-align:right;"><strong><?php echo $row_in_total[0]; ?></strong></td>
              </tr>
              <tr>
                <th align="right">Discount</th>
                <td style="text-align:right;"><strong><?php echo $discount; ?></strong></td>
              </tr>
              
              <tr>
                <th align="right">Total Amount</th>
                <td style="text-align:right;"><strong><?php echo  $row_in_total[0]  - $discount; ?></strong></td>
              </tr>

              <tr>
                <th align="right">Payment Mathod</th>
                <td style="text-align:right;"><strong><?php echo  $mathod; ?></strong></td>
              </tr>

              <tr>
                <th align="right">Shipping Cost</th>
                <td style="text-align:right;"><strong><?php echo  $shipping; ?></strong></td>
              </tr>
              <tr>
                <th align="right">Grand Total</th>
                <td style="text-align:right;"><strong><?php echo  $row_in_total[0]  - $discount + $shipping; ?></strong></td>
              </tr>
              
              
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="inp-vw.php?ItemID=<?php echo $orderID; ?>" target="_blank" class="btn btn-default pull-left">
          	<i class="fa fa-print"></i> Print</a>
          
        <a href="view_all_retail_invoices" class="btn btn-primary  pull-left" style="margin-left:15px;">
        	<i class="fa fa-reply" aria-hidden="true" ></i>&nbsp; Go Back</a>
        </div>
      </div>
    </section>
    <!-- /.content -->
    <div class="clearfix"></div>
  </div>