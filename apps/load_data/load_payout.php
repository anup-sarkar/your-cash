<?php	
require_once("../functions/functions.php");
testing_loggin();

 
$query = "select * from sd_payout WHERE  activity > 0 AND data_type = 2";

$res    = mysqli_query($mysqli,$query);
$count  = mysqli_num_rows($res);
$page = (int) (!isset($_REQUEST['pageId']) ? 1 :$_REQUEST['pageId']);
$page = ($page == 0 ? 1 : $page);
$recordsPerPage = 25;
$start = ($page-1) * $recordsPerPage;
$adjacents = "2";
    
$prev = $page - 1;
$next = $page + 1;
$lastpage = ceil($count/$recordsPerPage);
$lpm1 = $lastpage - 1;   
$pagination = "";
if($lastpage > 1)
    {   
        $pagination .= "<ul class='pagination pull-right'>";
        if ($page > 1)
            $pagination.= "<li><a href=\"all_payout_rceived#Page=".($prev)."\" onClick='changePagination(".($prev).");'>&laquo; Previous&nbsp;&nbsp;</a></li>";
        else
            $pagination.= "<li class='previous disabled'><a href='#'>&laquo; Previous&nbsp;&nbsp;</a></li>";   
        if ($lastpage < 7 + ($adjacents * 2))

        {   
            for ($counter = 1; $counter <= $lastpage; $counter++)
            {
                if ($counter == $page)
                    $pagination.= "<li class='active'><span>$counter</span></li>";
                else
                    $pagination.= "<li><a href=\"all_payout_rceived#Page=".($counter)."\" onClick='changePagination(".($counter).");'>$counter</a></li>";     
                         
            }
        }
        elseif($lastpage > 5 + ($adjacents * 2))
        {
            if($page < 1 + ($adjacents * 2))
            {
                for($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                {
                    if($counter == $page)
                        $pagination.= "<li class='active'><span>$counter</span></li>";
                    else
                        $pagination.= "<li><a href=\"all_payout_rceived#Page=".($counter)."\" onClick='changePagination(".($counter).");'>$counter</a></li>";     
                }
                $pagination.= "...";
                $pagination.= "<li><a href=\"all_payout_rceived#Page=".($lpm1)."\" onClick='changePagination(".($lpm1).");'>$lpm1</a></li>";
                $pagination.= "<li><a href=\"all_payout_rceived#Page=".($lastpage)."\" onClick='changePagination(".($lastpage).");'>$lastpage</a></li>";   
           
           }
           elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
           {
               $pagination.= "<li><a href=\"all_payout_rceived#Page=\"1\"\" onClick='changePagination(1);'>1</a></li>";
               $pagination.= "<li><a href=\"all_payout_rceived#Page=\"2\"\" onClick='changePagination(2);'>2</a></li>";
               $pagination.= "...";
               for($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
               {
                   if($counter == $page)
                       $pagination.= "<li class='active'><span>$counter</span></li>";
                   else
                       $pagination.= "<li><a href=\"all_payout_rceived#Page=".($counter)."\" onClick='changePagination(".($counter).");'>$counter</a></li>";     
               }
               $pagination.= "..";
               $pagination.= "<li><a href=\"all_payout_rceived#Page=".($lpm1)."\" onClick='changePagination(".($lpm1).");'>$lpm1</a></li>";
               $pagination.= "<li><a href=\"all_payout_rceived#Page=".($lastpage)."\" onClick='changePagination(".($lastpage).");'>$lastpage</a></li>";   
           }
           else
           {
               $pagination.= "<li><a href=\"Page=\"1\"\" onClick='changePagination(1);'>1</a></li>";
               $pagination.= "<li><a href=\"Page=\"2\"\" onClick='changePagination(2);'>2</a></li>";
               $pagination.= "..";
               for($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
               {
                   if($counter == $page)
                        $pagination.= "<li class='active'><span>$counter</span></li>";
                   else
                        $pagination.= "<li><a href=\"all_payout_rceived#Page=".($counter)."\" onClick='changePagination(".($counter).");'>$counter</a></li>";     
               }
           }
        }
        if($page < $counter - 1)
            $pagination.= "<li><a href=\"all_payout_rceived#Page=".($next)."\" onClick='changePagination(".($next).");'>Next &raquo;</a></li>";
        else
            $pagination.= "<li class='previous disabled'><a href='#'>Next &raquo;</a></li>";
        
        $pagination.= "</ul>";       
    }
    
if(isset($_POST['pageId']) && !empty($_POST['pageId']))
{
    $id=$_POST['pageId'];
}
else
{
    $id='0';
}
$query="select id from sd_payout order by id DESC
limit ".mysqli_real_escape_string($mysqli,$start).",$recordsPerPage";
//echo $query;
$res    =   mysqli_query($mysqli,$query);
$count  =   mysqli_num_rows($res);
$HTML='';

?>
 <div class="box-body table-responsive no-padding">
 <?php echo $pagination; ?>

	<table class="table table-hover">
			<thead>
             <tr class="info_member">
                                <th>ID</th>
                                <th>Date</th>
                                <th>Send From</th>
                                  <th>Mathod</th>
                                <th>Mathod Detail</th>
                                <th>Available Balance</th>
                                <th>Payout Amount</th>
                                <th>Activity</th>
                                <th>Action</th>
                                <th>Cancel</th>
                            </tr>
                        </thead>
                       <tbody>
			 <?php 		
                global $mysqli;
                $stmt = $mysqli->prepare("SELECT id, send_from, mathod, mobile_to, prev_due, amount, date, activity
						from sd_payout 
							WHERE  activity >  0 AND data_type = 2
                    ORDER BY activity ASC , id DESC limit ".mysqli_real_escape_string($mysqli,$start).",$recordsPerPage");
                $stmt->execute();    // Execute the prepared query.
                // get variables from result.
                $stmt->bind_result($id, $send_from, $mathod, $mbl_number, $prev_due, $amount,  $date, $activity);
				$stmt->store_result();
		
					while ($stmt->fetch()) { 
                           		if ($stmt_m = $mysqli->prepare("SELECT email, mobile from sd_client
                                    WHERE id = ? ")){
                                $stmt_m->bind_param('s', $send_from);  // Bind "$email" to parameter.
                                $stmt_m->execute();    // Execute the prepared query.
                                // get variables from result.
                                $stmt_m->bind_result($customr_name, $customer_mobile);
                                $stmt_m->store_result();
                                $stmt_m->fetch();
                                $stmt_m->close();
                                  }	
					?>               
                            <tr>
                                <td><?php echo $id; ?></td>
                                <td><?php echo $date; ?></td>
                                <td><?php echo $customr_name . '<br />' . $customer_mobile; ?></td>
                                  <td><?php echo $mathod; ?></td>
                                <td><?php echo $mbl_number; ?></td>
                                <td><?php echo $prev_due; ?></td>
                                <td><?php echo $amount; ?></td>
                                 <td style="text-align:center;"><?php if($activity == 2){echo"<b style='color: #e61900;'>Pending...";}if($activity == 3){echo"<b style='color: #008347;'>Approved </b>";}if($activity == 1){echo"canceled...";}?></b></td>
                                <td style="text-align:center;">
                                	<?php if($activity == 2){?>
                               		 <a href="confirmed_payment_agent/<?php echo $id; ?>" class="btn bg-orange " data-toggle="tooltip" data-placement="top" title="" data-original-title="Confirm This Bill" onClick="return confirm('Are you sure to Confirm This Transfer?')">Confirm Now<div class="ripple-container"></div></a>
                      		   		<?php } ?>
                                </td>
                                <td style="text-align:center;">
                                <?php if($activity == 2){?>
                               <a href="apps/bin_cat/delete_payout.php?actionID=view_all_bills&amp;billID=<?php echo $id; ?>" class="btn btn-danger btn-raised btn-xs" data-toggle="tooltip" data-placement="top" title="" onclick="return confirm('Are you sure to Delete this Row?')" data-original-title="Delete This Row"><i class="fa fa-close"></i><div class="ripple-container"></div></a>
                               <?php } ?>
                               
                      		    </td>
                            </tr>
   						 	 <?php }
							   $stmt->close();
								?>
                        </tbody>
                    </table>
                </div>
                

<?php echo $pagination; ?>
