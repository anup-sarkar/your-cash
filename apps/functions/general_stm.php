<?php
class edit_cat {

public function form_data() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_cat = urlencode($cat_name);
		
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT menu_id, menu_name, meta_desc, meta_key, page_cn, img1, img2, type FROM menu
			  WHERE menu_id = ?
				LIMIT 1");
			$stmt->bind_param('s', $url_string_cat);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($menu_id, $menu_name, $meta_desc, $meta_key, $page_cn, $img1, $img2, $type);
			$stmt->fetch();
			$stmt->close();
			
			if ($type == 1){ $type_selected1 = 'selected="selected"'; }  
			if ($type == 2){ $type_selected2 = 'selected="selected"'; }  
			echo 
			' 
			<div class="form-group">	  
			   <label>Category name <span class="red_star">*</span></label>
			    <input type="text" name="category_name" placeholder="Category name" value="'.$menu_name.'" class="form-control" required="required">
			    <input type="hidden" name="menu_id" placeholder="Category name" value="'.$menu_id.'" class="form-control" required="required">
			</div>
			
						 
			';
			} 
}
		
public function sub_cat_data() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_cat = urlencode($cat_name);
		
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT sub_menu_id, meta_desc, meta_key, page_cn, menu_id, sub_menu, img1, img2
				FROM sub_menu
			 		 WHERE sub_menu_id = ?
				LIMIT 1");
			$stmt->bind_param('s', $url_string_cat);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($sub_menu_id, $meta_desc, $meta_key, $page_cn, $menu_id, $sub_menu, $img1, $img2);
			$stmt->fetch();
			$stmt->close();

			 global $mysqli;
			 $stmt_m = $mysqli->prepare("SELECT menu_id, menu_name 
			 		FROM menu 
						  ORDER BY menu_id ASC");
			 $stmt_m->execute();
			 $stmt_m->bind_result($mn_id, $menu_name);
   			
			echo 
			'<input type="hidden" name="pro_img1" placeholder="Item name" value="'.$img1.'" class="form-control" required="required">
			<input type="hidden" name="pro_img2" placeholder="Item name" value="'.$img2.'" class="form-control" required="required">
			
				   <div id="upload_area" class="corners align_center"></div>
                       <div class="form-group">  
						   <img src="../images/products/'.$img2.'" width="60" alt="img" />
                      
					 </div> 
				
				<label>Sub Category name <span class="red_star">*</span></label>
			    <input type="text" name="sub_menu" placeholder="Category name" value="'.$sub_menu.'" class="form-control" required="required">
			    <input type="hidden" name="sub_menu_id" placeholder="Category name" value="'.$sub_menu_id.'" class="form-control" required="required">
				
 				 <div class="form-group">
                     <label>Category name <span class="red_star">*</span></label>
                          <select name="cat" id="drop1" class="form-control" required>
                              <option value="">Select A Category Name</option>';
								      while ($stmt_m->fetch()) {
            							  echo "<option value='$mn_id'"; if ($menu_id == $mn_id){ echo 'selected="selected"'; } echo ">$menu_name</option>";
											  }
    								 $stmt_m->close();   
                                 echo '</select>
                  </div>
				  
				  
				  <div class="form-group col-md-12">
							 <label>Meta description  </label>
							 <textarea type="text" name="meta_desc" rows="5" class="form-control"> '.$meta_desc.'</textarea>
						</div>
					 
						<div class="form-group col-md-12">
							 <label>Meta keywords  </label>
							 <textarea type="text" name="meta_keywords" rows="5" class="form-control"> '.$meta_key.'</textarea>
						</div>
						
						<div class="form-group col-md-12">
                                <label>Page Footer Details</label>
								<textarea name="editor1"  class="input-style" id="ditor">'.$page_cn.'</textarea>
								 <script>
								  CKEDITOR.replace( "ditor", {
									fullPage: true,
									allowedContent: true,
									extraPlugins: "wysiwygarea"
								  });
							
								</script>        
					  </div>
 				  	
			';
			}
}


public function third_sub_cat_data() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_cat = urlencode($cat_name);
		
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT id, name, meta_desc, meta_key, page_cn,  img1, img2
			 from sd_third_sub
			 	 WHERE id = ?
				LIMIT 1");
			$stmt->bind_param('s', $url_string_cat);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($t_id, $t_menu, $meta_desc, $meta_key, $page_cn, $img1, $img2);
			$stmt->fetch();
			$stmt->close();
			global $mysqli;
			 $stmt_m = $mysqli->prepare("SELECT menu_id, menu_name 
			 		FROM menu 
						  ORDER BY menu_id ASC");
			 $stmt_m->execute();
			 $stmt_m->bind_result($mn_id, $menu_name);
   			
			echo 
			'
			 
					 
			   <label>Category name <span class="red_star">*</span></label>
			    <input type="text" name="t_menu" placeholder="Category name" value="'.$t_menu.'" class="form-control" required="required">
			    <input type="hidden" name="sub_menu_id" placeholder="Category name" value="'.$t_id.'" class="form-control" required="required">
			
			
			  <div class="form-group col-md-12">
							 <label>Meta description  </label>
							 <textarea type="text" name="meta_desc" rows="5" class="form-control"> '.$meta_desc.'</textarea>
						</div>
					 
						<div class="form-group col-md-12">
							 <label>Meta keywords  </label>
							 <textarea type="text" name="meta_keywords" rows="5" class="form-control"> '.$meta_key.'</textarea>
						</div>
						
						<div class="form-group col-md-12">
                                <label>Page Footer Details</label>
								<textarea name="editor1"  class="input-style" id="ditor">'.$page_cn.'</textarea>
								 <script>
								  CKEDITOR.replace( "ditor", {
									fullPage: true,
									allowedContent: true,
									extraPlugins: "wysiwygarea"
								  });
							
								</script>        
					  </div>
			
			';
			}
}		
		
	
public function add_item() 
	{
	 
				echo 
					' 	 
				   <div class="form-group col-md-4">
                        <label>Task name <span class="red_star">*</span></label>
                        <input type="text" name="item_name" placeholder="Task name" value="" class="form-control" required="required">
                      </div>


                      
					 
					  						
					<div class="form-group col-md-4">
                      <label>Task Amount (if completed) <span class="red_star">*</span></label>
                      <input type="text" name="price" placeholder="E.g 200" value="" class="form-control" required="required">
                     </div>
					
				 
							
					<div class="form-group col-md-4">
                      <label>Generation Amount <span class="red_star">*</span></label>
                      <input type="text" name="discount_price" placeholder="E.g 200" value="" class="form-control" required="required">
                     </div>
							
			 
					
					


                    <div class="form-group col-md-4">
                      <label>Step Title Name  </label>
                      <input type="text" name="stepName" placeholder="Give Step Name" value="" class="form-control"  >
                     </div>
					
					 <div class="form-group col-md-4">
                      <label>Step Details </label>
                      <input type="text" name="stepDetails" placeholder="Step description" value="" class="form-control"  >
                     </div>
					

					 <div class="form-group col-md-4">
                      <label>Step Link  </label>
                      <input type="text" name="link" placeholder="Step Website Link" value="" class="form-control"  >
                     </div>




                     <div class="form-group col-md-4">
                      <label>Step Type </label>
                      <select name="type" class="form-control">
                  
                      	<option value="1">Website</option>
                      	<option value="2" >Newspaper</option>
                      	<option value="3">Youtube</option>
                      	<option value="4">Other</option>
                      
                      </select>
                     </div>
					
				 

					 <div class="form-group col-md-4">
                      <label>Step Sorting Order  </label>
                      <input type="number" name="order" placeholder="Step Sorting Order" value="" class="form-control"  >
                     </div>







                    <div class="form-group col-md-4">
                      <label>Interstitial Ad Code  </label>
                      <input type="text" name="inter" placeholder="Interstitial App ID" value="" class="form-control"  >
                     </div>
					
					 <div class="form-group col-md-4">
                      <label>Banner Ad Code  </label>
                      <input type="text" name="banner" placeholder="Banner App ID" value="" class="form-control"  >
                     </div>
					

					 <div class="form-group col-md-4">
                      <label>Click Ad Code  </label>
                      <input type="text" name="click" placeholder="Click App ID" value="" class="form-control"  >
                     </div>
					

					  <div class="form-group col-md-4"> 
                       <label>Activity <span class="red_star">*</span></label>
					       <label>
						  <input type="radio" name="activity" value="1" id="featured_0" checked="checked" />
						  Yes</label>
					   
						<label>
						  <input type="radio" name="activity" value="0" id="featured_1"   />
						  No</label>
                    </div>
						   
	 

					   
				 
					
					';
}
		
public function update_product() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_item = urlencode($cat_name);
			
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT id, item_name,  price, generation, date, up_date, activity2 ,interID,bannerID,clickID,StepTitle,StepDetails,StepLink,StepType,SortOrder
			FROM sd_item_l
			  WHERE id = ?
				LIMIT 1");
			$stmt->bind_param('i', $url_string_item);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($id, $item_name, $sell, $generation, $date, $up_date,  $activity,$inter,$banner,$click,$stepName,$stepDetails,$link,$type,$order);
			$stmt->fetch();
			$stmt->close();
			
			$text="";
			  
			 if ($up_date == NULL)
			 		{ 
						$last_updated = $date; 
					} 
				else 
					{ 
						$last_updated = $up_date;
					}
			 if($type == 1)
			 {
			 	$text="Website";
			 }elseif ($type==2) {
			 	# code...
			 	$text="Newspaper";
			 }
			 elseif ($type==3) {
			 	# code...
			 	$text="Youtube";
			 }
			 else
			 {
			 	$type=0; 
			 	$text="Other";
			 }


			echo 
			' 
			
			<div class="form-group">
             <input type="hidden" name="id" placeholder="Item name" value="'.$id.'" class="form-control" required="required">
			  
					 
				 <div style="clear:both;"></div>
									
					  
							
                       <div class="form-group col-md-4">
                        <label>Task name <span class="red_star">*</span></label>
                        <input type="text" name="item_name" placeholder="Task name" value="'.$item_name.'" class="form-control" required="required">
                      </div>


                      
					 
					  						
					<div class="form-group col-md-4">
                      <label>Task Amount (if completed) <span class="red_star">*</span></label>
                      <input type="text" name="price" placeholder="E.g 200" value="'.$sell.'" class="form-control" required="required">
                     </div>
					
				 
							
					<div class="form-group col-md-4">
                      <label>Generation Amount <span class="red_star">*</span></label>
                      <input type="text" name="discount_price" placeholder="E.g 200" value="'.$generation.'" class="form-control" required="required">
                     </div>
							
			 
					
					


                    <div class="form-group col-md-4">
                      <label>Step Title Name  </label>
                      <input type="text" name="stepName" placeholder="Give Step Name" value="'.$stepName.'" class="form-control"  >
                     </div>
					
					 <div class="form-group col-md-4">
                      <label>Step Details </label>
                      <input type="text" name="stepDetails" placeholder="Step description" value="'.$stepDetails.'" class="form-control"  >
                     </div>
					

					 <div class="form-group col-md-4">
                      <label>Step Link  </label>
                      <input type="text" name="link" placeholder="Step Website Link" value="'.$link.'" class="form-control"  >
                     </div>




                     <div class="form-group col-md-4">
                      <label>Step Type </label>
                      <select name="type" class="form-control">
                      <option value="'.$type.'">'.$text.'</option>
                      	<option value="1">Website</option>
                      	<option value="2" >Newspaper</option>
                      	<option value="3">Youtube</option>
                      	<option value="4">Other</option>
                      
                      </select>
                     </div>
					
				 

					 <div class="form-group col-md-4">
                      <label>Step Sorting Order  </label>
                      <input type="text" name="order" placeholder="Step Sorting Order" value="'.$order.'" class="form-control"  >
                     </div>







                    <div class="form-group col-md-4">
                      <label>Interstitial Ad Code  </label>
                      <input type="text" name="inter" placeholder="Interstitial App ID" value="'.$inter.'" class="form-control"  >
                     </div>
					
					 <div class="form-group col-md-4">
                      <label>Banner Ad Code  </label>
                      <input type="text" name="banner" placeholder="Banner App ID" value="'.$banner.'" class="form-control"  >
                     </div>
					

					 <div class="form-group col-md-4">
                      <label>Click Ad Code  </label>
                      <input type="text" name="click" placeholder="Click App ID" value="'.$click.'" class="form-control"  >
                     </div>
					

					  <div class="form-group col-md-4"> 
                       <label>Activity <span class="red_star">*</span></label>
					       <label>
						  <input type="radio" name="activity" value="1" id="featured_0"'; if ($activity == 1){echo 'checked="checked"';} echo '/>
						  Yes</label>
					   
						<label>
						  <input type="radio" name="activity" value="0" id="featured_1"'; if ($activity == 0){echo 'checked="checked"';} echo '  />
						  No</label>
                    </div>
						   
			  Last Modified: '.$last_updated.''
					   ;
			}
	}	
	

public function add_more_img() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_item = urlencode($cat_name);
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT id FROM sd_item_l
			  WHERE id = ?
				LIMIT 1");
			$stmt->bind_param('i', $url_string_item);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($id);
			$stmt->fetch();
			$stmt->close();
			
	
			echo 
			' 
		<div class="form-group">
             <input type="hidden" name="pro_id" placeholder="Item ID" value="'.$id.'" class="form-control" required="required">
			
			<div id="upload_area" class="corners align_center"></div>
       
        </div>';
	}
}	
		

public function add_slide() 
	{

	global $mysqli;
    $stmt = $mysqli->prepare("SELECT menu_id, menu_name FROM menu 
                  ORDER BY menu_id ASC");
     $stmt->execute();
     $stmt->bind_result($id, $menu_name);
	 
	 echo 
		'
		 <div class="form-group">
            <label>Category name <span class="red_star">*</span></label>
               <select name="cat" id="drop1" class="form-control" required>
                  <option value="">Select A Category Name</option>';
            					  echo "<option value='999999989'>Home</option>";
					      while ($stmt->fetch()) {
            					  echo "<option value='$id'>$menu_name</option>";
									  }
    						 $stmt->close();   
                     echo '</select>
          </div>

		<div class="form-group">
             <label>URL <span class="red_star">*</span></label>
             <input type="text" name="title_one" class="form-control" value="#" required="required">
        </div>
		';
	}	

public function update_slide() 
	{
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_item = urlencode($cat_name);
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT id, title_one, img1, img2, cat from sd_slide_mng
			  WHERE id = ?
				LIMIT 1");
			$stmt->bind_param('i', $url_string_item);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($id, $title_one, $img1, $img2, $cat);
			$stmt->fetch();
			$stmt->close();	


	global $mysqli;
    $stmt = $mysqli->prepare("SELECT menu_id, menu_name FROM menu 
                  ORDER BY menu_id ASC");
     $stmt->execute();
     $stmt->bind_result($menu_id, $menu_name);

	echo 
		' 
		<div class="form-group">
         <input type="hidden" name="id" placeholder="Item name" value="'.$id.'" class="form-control" required="required">
		 <input type="hidden" name="pro_img1" placeholder="Item name" value="'.$img1.'" class="form-control" required="required">
		 <input type="hidden" name="pro_img2" placeholder="Item name" value="'.$img2.'" class="form-control" required="required">
	
				<div id="upload_area" class="corners align_center"></div>
                     <img src="../images/slide/'.$img1.'" width="60" alt="img" />
                 </div>

			 <div class="form-group">
                     <label>Category name <span class="red_star">*</span></label>
                          <select name="cat" id="drop1" class="form-control" required>
                              <option value="">Select A Category Name</option>';
            					  echo "<option value='999999989'";
								 if ($cat == '999999989')
								 		{
								 echo "selected='selected'";
								 		} 
										echo ">Home</option>";
							while ($stmt->fetch()) 
											{
            					 echo "<option value='$menu_id'"; 
								 if ($cat == $menu_id)
								 		{
								 echo "selected='selected'";
								 		} 
										echo ">$menu_name</option>";
											}
    							 	 $stmt->close();   
                                 echo '</select>
                  </div>


		<div class="form-group">
             <label>URL <span class="red_star">*</span></label>
             <input type="text" name="title_one" value="'.$title_one.'" class="form-control" required="required">
           </div>';
}	
	
public function update_more_post() 
	{
	if (isset($_GET['ItemID'])) { 
		$sanitize = true;
		$cat_name = isset($_GET['ItemID']) ? $_GET['ItemID'] : '';
		$url_string_item = urlencode($cat_name);
			global $mysqli;
			$stmt = $mysqli->prepare("SELECT id, title, full_desc, img1, img2 FROM sd_posts
			  WHERE id = ?
				LIMIT 1");
			$stmt->bind_param('i', $url_string_item);  // Bind "$email" to parameter.
			$stmt->execute();    // Execute the prepared query.
			// get variables from result.
			$stmt->store_result();
			$stmt->bind_result($id, $title,	$full_desc, $img1, $img2);
			$stmt->fetch();
			$stmt->close();
			
						 
			 if ($img1 == NULL){ $image = "photo.png"; } else { $image = $img1;}
			 if ($up_date == NULL)
			 		{ 
						$last_updated = $date; 
					} 
				else 
					{ 
						$last_updated = $up_date;
					}
			 
			echo 
			' 
		<div class="form-group">
            <input type="hidden" name="id" placeholder="Item name" value="'.$id.'" class="form-control" required="required">			
							
						  <div class="form-group">
                               <label>Title </label>
                               <input type="text" name="title" placeholder="Item Code" value="'.$title.'" class="form-control">
                            </div>

                     
							<div class="form-group">
                                <label>Details</label>
								<textarea name="editor1"  class="input-style" id="ditor">'.$full_desc.'</textarea>
								 <script>
								  CKEDITOR.replace( "ditor", {
									fullPage: true,
									allowedContent: true,
									extraPlugins: "wysiwygarea"
								  });
							
								</script>


                            </div>';							   ;
			}
	}		
	
	
}
$data_out = new edit_cat();
?>
