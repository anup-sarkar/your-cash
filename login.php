<?php
include_once 'apps/functions/functions.php';


if (login_check($mysqli) == true) {
    header("Location:index.php");
	exit;
} else {
    $logged = 'out';
}

 
?>
 <!DOCTYPE HTML>
 <html>
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>YourCash Admin Panel</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="YourCash" />
  <meta name="keywords" content="YourCash" />
  <meta name="author" content="YourCash" />

    <!-- Facebook and Twitter integration -->
  <meta property="og:title" content=""/>
  <meta property="og:image" content=""/>
  <meta property="og:url" content=""/>
  <meta property="og:site_name" content=""/>
  <meta property="og:description" content=""/>
  <meta name="twitter:title" content="" />
  <meta name="twitter:image" content="" />
  <meta name="twitter:url" content="" />
  <meta name="twitter:card" content="" />

 <link href="https://fonts.googleapis.com/css?family=Droid+Sans" rel="stylesheet">

 
  <!-- Animate.css -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Popper JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag ---------->
  <!-- FOR IE9 below -->
  <!--[if lt IE 9]>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  </head>
  <body>
 
 <link rel="stylesheet" href="http://keith4president.info//assets/css/signin.css">
 
  <div class="text-center" style="margin: 0 auto">

 <form action="includes/process_login.php" method="post" name="login_form" class="form" role="form">   
        

<img class="mb-4" src="dist/img/logo.png" alt="" width="152" height="152">
      <h1 class="h3 mb-3 font-weight-bold"  ><b>WEB ADMIN</b> LOGIN</h1>
      
      <label for="inputEmail" class="sr-only" >Email address</label>
      <input type="email" name="email" placeholder="Email" class="form-control" value="" required autofocus>
       <span class="text-danger"></span>
                                        
                                        
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password"   class="form-control" placeholder="Password" id="l_pass" name="password" class="form-control" value=""  required>
        <span class="text-danger"></span>
                                   
      <div class="checkbox mb-3">
        <br/>
               <?php
    if (isset($_GET['error'])) {
       echo '<div class="alert  alert-danger" > <h6>Username OR Password Not Matched!</h6> </div>';
         }
  ?> 
        
      </div>
      <button class="btn btn-lg btn-success btn-block" type="submit" style="background-color: #2e6069" onclick="formhash(this.form, this.form.password);" ><i class="fa fa-user-circle"></i> <b>SIGN IN</b> </button>

    
      <p class="mt-5 mb-3 text-muted">All Right Reserved   &copy; 2018-2019</p>

     </form>     
     </div>
 
 


 
     
    <script type="text/JavaScript" src="js/sha512.js"></script> 
    <script type="text/JavaScript" src="js/forms.js"></script>
 
  
  </body>
</html>

