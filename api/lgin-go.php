<?php
require_once("app-hdr.php");

$data = json_decode(file_get_contents("php://input"));
 
 json_encode(
    array("message" => $data->username." ".$data->password." ".$data->imei)
);
 
		$json = array();
		$email = $data->username;
		$password_rcv = $data->password;
		$imei = $data->imei;
		$password = md5($password_rcv);
		
		if ($stmt = $mysqli->prepare("SELECT id, mobile 
        FROM sd_client
			WHERE  mobile = ? AND pass_hash = ?
        LIMIT 1")) {
        $stmt->bind_param('ss', $email, $password);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
 
        // get variables from result.
        $stmt->bind_result($user_id, $username);
        $stmt->fetch();
		
 	 
   if ($stmt->num_rows == 1) {
    
    	if ($stmt_deactv = $mysqli->prepare("SELECT id, mobile 
        FROM sd_client
			WHERE activity = 1 AND mobile = ? AND pass_hash = ?
        LIMIT 1")) {
        $stmt_deactv->bind_param('ss', $email, $password);  // Bind "$email" to parameter.
        $stmt_deactv->execute();    // Execute the prepared query.
        $stmt_deactv->store_result();
 
        // get variables from result.
        $stmt_deactv->bind_result($user_id, $username);
        $stmt_deactv->fetch();
		
  if ($stmt_deactv->num_rows > 0) {	
      
      
		if ($stmt_imei = $mysqli->prepare("SELECT id 
        FROM sd_client
			WHERE activity = 1 AND imei = ?
        LIMIT 1")) {
        $stmt_imei->bind_param('s', $imei);  // Bind "$email" to parameter.
        $stmt_imei->execute();    // Execute the prepared query.
        $stmt_imei->store_result();
 
        // get variables from result.
        $stmt_imei->bind_result($user_id);
        $stmt_imei->fetch();
		
  if ($stmt_imei->num_rows > 0) {	
	  
	  if ($stmt_mm = $mysqli->prepare("SELECT id 
                             FROM sd_block 
                             WHERE user = ?  ")) {
        $stmt_mm->bind_param('i', $user_id);
 
        // Execute the prepared query. 
        $stmt_mm->execute();
        $stmt_mm->store_result();
 
			if ($stmt_mm->num_rows > 0) {
				 $json['messageID'] = 3;
        		 $json['message'] = 'ID Has Been Blocked. Please Contact to admin!';
			
			}
		 else{

		  
			 // Login success 
			 $json['memberID'] = $user_id;
			 $json['messageID'] = 1;
			 $json['message'] = 'Login Successful!';
			}
		} 
		 
  
  }
  else {
	  
	   $json['messageID'] = 3;
       $json['message'] = 'IMEI Not Matched. Please Contact to admin!';
	  
    	}
 	}
   // Close IMEI Check 	
 	
  } 
  
  else {
        // Login failed 
         $json['messageID'] = 0;
         $json['message'] = 'ID De-Activated. Please Contact to admin!';
    }
    
  }   
   // Close User Activity Check 
 	
 	
 	
  } 
  
  else {
        // Login failed 
         $json['messageID'] = 0;
         $json['message'] = 'Username OR Password did not matched!';
    }
	
}	
 
     echo json_encode($json);
     
 	 
	
?>