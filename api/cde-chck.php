<?php
require_once("app-hdr.php");

$data = json_decode(file_get_contents("php://input"));
 
 json_encode(
    array("message" => $data->code." ".$data->imei)
);
  
		 $json = array();
		$code = $data->code;
		$mobile = $data->mobile;
 		$iemi = $data->imei;
		$en_date = date('Y-m-d');

	  if ($stmt_m = $mysqli->prepare("SELECT name, email, mobile, password, salt, ref, date
    	    FROM sd_client_req
     			  WHERE sessionID = ?
        ORDER BY id DESC LIMIT 1 ")) {
        $stmt_m->bind_param('s', $iemi);  // Bind "$email" to parameter.
        $stmt_m->execute();    // Execute the prepared query.
        $stmt_m->store_result();
        // get variables from result.
        $stmt_m->bind_result($user_name, $user_email, $user_mobile, $password, $cnf_password, $ref, $date);
        $stmt_m->fetch();
		$stmt_m->close();
		}
		
		 if ($stmt_m = $mysqli->prepare("SELECT id
    	    FROM sd_client
     			  WHERE mobile = ?
        ORDER BY id DESC LIMIT 1 ")) {
        $stmt_m->bind_param('s', $ref);  // Bind "$email" to parameter.
        $stmt_m->execute();    // Execute the prepared query.
        $stmt_m->store_result();
        // get variables from result.
        $stmt_m->bind_result($refID);
        $stmt_m->fetch();
		$stmt_m->close();
		}
		
		
	  if ($stmt = $mysqli->prepare("SELECT id, mobile 
        FROM sd_client_req
     	  WHERE activity = 1 AND security_code = ? AND mobile = ?
        LIMIT 1")) {
        $stmt->bind_param('ss', $code, $user_mobile);  // Bind "$email" to parameter.
        $stmt->execute();    // Execute the prepared query.
        $stmt->store_result();
        // get variables from result.
        $stmt->bind_result($user_id, $username);
        $stmt->fetch();
		
	 if ($stmt->num_rows == 1) {
			
			  // Insert the new user into the database 
        if ($insert_stmt = $mysqli->prepare("INSERT INTO sd_client (name, email, mobile, password, pass_hash, ref, ref_id, imei, date) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
            $insert_stmt->bind_param('sssssssss', $user_name, $user_email, $user_mobile,  $password, $cnf_password, $ref, $refID, $iemi, $date);
            // Execute the prepared query.
            if (! $insert_stmt->execute()) {
                header('Location: ../error.php?err=Registration failure: INSERT');
            }
        }
		
		global $mysqli;
		if ($update_stmt = $mysqli->prepare("UPDATE sd_client_req SET activity = 0 WHERE sessionID = ? LIMIT 1")){
		// Bind the variables:
		$update_stmt->bind_param('s', $iemi);
		// Execute the query:
		
		if (!$update_stmt->execute()) {
					$json['message'] = 'Query Not Matched!';
			   }
		}
 		
		$json['message'] = 1;
	}
 
		 
		 else
		  {
            // No user exists.
      		$json['message'] = 'Verification Code Did Not Matched!';
        }
        
    }




  
     echo json_encode($json);
     
 	 
	
?>