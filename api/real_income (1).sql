-- phpMyAdmin SQL Dump
-- version 2.11.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2018 at 03:03 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `real_income`
--

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(9, '1464783533'),
(9, '1465768623'),
(9, '1465768901'),
(9, '1465768902'),
(9, '1465768931'),
(9, '1466483834'),
(9, '1466483955'),
(9, '1466966382'),
(9, '1467170448'),
(36, '1513320774'),
(9, '1517467973'),
(42, '1521625308'),
(46, '1522672317');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `cID` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`, `cID`) VALUES
(9, 'alamin2', 'alamin@gmail.com', '3f508a451b933b70cd3bbc3136de4b48e1050cf7e4d1c2f20aedde87b95e92d31dd69dd8bb4da62ebc9ebde8ac7967421c825a010236e4b10bddc2e221b74cab', '85eccd4f797b7da01842a094d113c17fc20bcf2683c6afcc53feece2725171c7e3abb23179f0802f012fd13e751a760356565d53f8da88cdc032f0efe43f5888', 'be714720153b250');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL auto_increment,
  `menu_name` varchar(50) character set utf8 NOT NULL,
  `meta_desc` text,
  `meta_key` text,
  `page_cn` text,
  `img1` varchar(500) default NULL,
  `img2` varchar(500) default NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY  (`menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `meta_desc`, `meta_key`, `page_cn`, `img1`, `img2`, `type`) VALUES
(41, 'Women&#39;s Fashion', NULL, NULL, NULL, '', '', 1),
(42, 'Men&#39;s Fashion', NULL, NULL, NULL, '', '', 1),
(43, 'Phones & Tablets', NULL, NULL, NULL, NULL, NULL, 1),
(45, 'Sports & Travel', NULL, NULL, NULL, NULL, NULL, 1),
(47, 'Baby, Kids & Toys', NULL, NULL, NULL, NULL, NULL, 1),
(48, 'Other Categories', NULL, NULL, NULL, NULL, NULL, 1),
(50, 'BEAUTY & HEALTH', NULL, NULL, NULL, 'thumb3-health-beauty-top-banner-1517976992.jpg', 'thumb2-health-beauty-top-banner-1517976992.jpg', 1),
(52, 'Laptop', NULL, NULL, NULL, 'thumb3-banner_02-1519708845.jpg', 'thumb2-banner_02-1519708845.jpg', 1),
(53, 'demo cat one', ' ', ' ', '', NULL, NULL, 1),
(54, 'MOTHER’S DAY SPECIAL', ' ', ' ', '', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sales_person`
--

CREATE TABLE `sales_person` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `cID` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `sales_person`
--

INSERT INTO `sales_person` (`id`, `username`, `email`, `password`, `salt`, `cID`) VALUES
(10, 'easycom', 'alamin@gmail.com', '4b3fbce13d3e2b361f8ea03c2958ba2997d8c05cd0bff8b4c1b4ca7ccac2f4cddf51363014bf2c4fceff17f975d87e920db09d4510e44dc38ff66703a4add5e0', '01bc7a0dcb2a682ab8fd0e5c22fa042f3f076e4876bc01710652701bca939012e53f3962429c1904c2439b47c9d299090bfbff696c0c6a4512b17b3187cc5cc4', ''),
(11, 'demoVai', 'demo21@stardesignbd.com', 'cf979e07b8ea512fe24fbca34e78c18861ddc4e57093ca68ea008ed7477aecc190f26085129e91c3a19376fc19774a906eb0a7f5d43787585aafb1c5b79035af', '6ac915efef6794ef24d66c315f89b461a7b1ff1c7240b00ede838227fcb67a0770cbddc4f998c6cf8b75c0e7a3b4d9ed534f756fe2de8acba82e8a6c7efba3da', ''),
(12, 'Sharmin', 'shanjedul@live.com', 'c9a30641cdf26370957d6bcb2d8686792bad75923d4a69c73d14819bc353bbf4b559f9088dc28ac81b1cb70552c85d0db547345da9d6863ecabe785e87cb0023', '381982e6d6de40f7ab6277b7b6cacb6e08a01eb48f7092684dd2474c3407a19ba0a60f3178b4194b0131a337d9cacff903514b09e16ac350777f5f55095663e1', '');

-- --------------------------------------------------------

--
-- Table structure for table `sd_ads`
--

CREATE TABLE `sd_ads` (
  `id` int(11) NOT NULL auto_increment,
  `image` text,
  `url` text,
  `adsence` text,
  `position` int(11) NOT NULL,
  `p_1` int(11) default '0',
  `p_2` int(11) default '0',
  `p_3` int(11) default '0',
  `p_4` int(11) default '0',
  `p_5` int(11) default '0',
  `p_6` int(11) default '0',
  `date` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `sd_ads`
--

INSERT INTO `sd_ads` (`id`, `image`, `url`, `adsence`, `position`, `p_1`, `p_2`, `p_3`, `p_4`, `p_5`, `p_6`, `date`) VALUES
(27, '772279db173384fdd08f39ea2830f967_1446312150_1446312987_1468086366.gif', '#', '', 0, 1, NULL, NULL, NULL, 1, 1, '2016-07-09'),
(28, 'Ad_2_1446377910_1468086418.png', '#', '', 0, NULL, NULL, NULL, NULL, 1, 1, '2016-07-09'),
(29, '11165058226993752067_1467443959_1468087496.gif', '#', '', 0, NULL, 1, NULL, 1, NULL, NULL, '2016-07-09'),
(30, '1032713556723859918_1467441389_1468087521.gif', '#', '', 0, NULL, NULL, 1, NULL, 1, NULL, '2016-07-09'),
(31, '10825628125623918228_1468326783.jpg', '#', '', 0, 1, NULL, NULL, NULL, NULL, 1, '2016-07-12');

-- --------------------------------------------------------

--
-- Table structure for table `sd_brands`
--

CREATE TABLE `sd_brands` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(400) NOT NULL,
  `img1` varchar(500) NOT NULL,
  `img2` varchar(500) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sd_brands`
--

INSERT INTO `sd_brands` (`id`, `title`, `img1`, `img2`) VALUES
(1, 'INTEL', 'thumb3-logo-intel-1467454618.png', 'thumb2-logo-intel-1467454618.png'),
(2, 'Firm House', 'thumb3-thumb3-4-1446718054-1468076495.jpg', 'thumb2-thumb3-4-1446718054-1468076495.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sd_cart`
--

CREATE TABLE `sd_cart` (
  `id` int(11) NOT NULL auto_increment,
  `task` varchar(20) NOT NULL,
  `lst_sl` varchar(20) NOT NULL,
  `user` varchar(20) NOT NULL,
  `date` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sd_cart`
--

INSERT INTO `sd_cart` (`id`, `task`, `lst_sl`, `user`, `date`) VALUES
(1, '1', '1', '5', '2018-07-16'),
(2, '1', '2', '5', '2018-07-15');

-- --------------------------------------------------------

--
-- Table structure for table `sd_client`
--

CREATE TABLE `sd_client` (
  `id` int(11) NOT NULL auto_increment,
  `category` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `l_name` varchar(100) default NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(50) default NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `address` text NOT NULL,
  `ref` varchar(15) default '0',
  `imei` varchar(500) NOT NULL,
  `location` varchar(100) default NULL,
  `img1` varchar(500) default 'photo.png',
  `img2` varchar(500) default 'photo.png',
  `type` int(11) NOT NULL default '1',
  `found_us` varchar(100) default NULL,
  `activity` int(11) NOT NULL default '1',
  `date` varchar(100) NOT NULL,
  `up_date` varchar(100) NOT NULL,
  `online` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `mobile` (`mobile`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

--
-- Dumping data for table `sd_client`
--

INSERT INTO `sd_client` (`id`, `category`, `username`, `name`, `l_name`, `mobile`, `email`, `password`, `salt`, `address`, `ref`, `imei`, `location`, `img1`, `img2`, `type`, `found_us`, `activity`, `date`, `up_date`, `online`) VALUES
(43, '', '', 'Robiul Karim', NULL, '01569254441', 'rabbi@gmail.com', '68e95a7cdf8c5eb5fa0e56edf75138852346ee4fdf748c3bdda0be6965917b0992575408d99386ee24042eaf69298e797981bc5567807a2ffb734444b94e0f20', '12a0b259c7b0a4d77fca8f618d6d2d1f849018fd3fd15d3d6b7ea6addd025c8e4c879b18b3d6b2a59e52251e8804bfe626743f962e13eb69d491f47f6cf3dc57', 'adorsonogor\r\nlink road', '0', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-03-21', '', 0),
(44, '', '', 'shamim', NULL, '01918163449', 'shamim@gmail.com', 'a7b08b7b0bc0b3f22ab64b1f7ecdb68ba6576d4a0c3db24b86875a004c3cefae604b4f3721c4c5c76eb423cc1ad5b16be1eade7f8a201d0c5100a385cf2e1e9a', '0b276d7fe3dd18bd076d06003e63c23085292b0ebd4567e8e451d4c467211d5d7dd446ae9c678712aceb403a3cd7c7c011103c83b4ba9a8489e5cb14aa20e0f2', 'adasd asd asdada', '0', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-04-02', '', 0),
(45, '', '', 'demo2', NULL, '01951263254', 'demo2@gmail.com', 'b26ef12e9ef43d9310e24a6457d47c0025981db4fa59737b4afcc0386a88aa4f8d74bdcef517ebc18a9e943ac046fd6911fb9e6a1e2ab0d0a5f3b67e940379eb', '25fda7e2f20ce58000d291617ba42be1b3f2307e8486e6d44e57c6431cc3d9fb7b6edf8f29a7611e8b534338f93029df7948a5bf811db2d592ed3c2fb54d6f83', 'ssdf dsfsdf', '01782882820', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-04-02', '', 0),
(46, '', '', 'shamim Hossaain', NULL, '01820762264', 'info@example.com', '066eefac6add997090ac89da4b90b16bcf641b8e0ffeefa3514aa5bd0d50ccd24f8fefeeb14ec3223129b88626735dd0661bd1a99aa0a111b40afb7919a6a093', 'eec4b2a22ccfbf03dbdad6e1a96c4ae519bee68641a9620037925855a408b6e698925f03f499ec2f5a0e22c510f50ed7999a3a99e0a353a83793bdf0fb28b8ba', 'Badda', '01918163449', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-04-02', '', 0),
(47, '', '', 'shamim', NULL, '017258552545', '', '8752596caf3ccdfab7a1664be2844cdeb38ee7d73e9f756ec6b694e3ce1664351176405b8e6ebdbc9e0e99e7297f6f7ae8898cb83c668c533c6cb765fcc4cc29', 'd659f71f01871442fefe5dde4689332461b5ee01a52bbca331b0fc22f08f499a5e24a5dead20094f1efb3b15c9987ae545c103de43147aac0d40870c585d573f', 'sdf ssdf', '01782882820', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-04-02', '', 0),
(48, '', '', 'affilate user', NULL, '01782882820', 'affilate@gmail.com', '29b8b0875a9e6cb8e7dfcc38ca1bd799ed821434d163a23ba7897a1821719cd07bddc45d8f83563813afbc7b822a5cd0b737a498629c0f87494bb5fb856effff', '4378c93d7b478db27c43a7eae5395a1a8618d795c0d328a4a6ad8bb8df3c876cac452c70672ab1e2c1eac66caf4f3fc6f2c0011965a7d2a72e057e7c087f4f24', 'sdfsdf', '01673842613', '', NULL, 'photo.png', 'photo.png', 2, NULL, 1, '2018-04-20', '', 0),
(49, '', '', 'test2', NULL, '01673842613', 'test2@gmail.com', 'b39e31b0aea1b07db7a36aedbb2d8e785f17977321d6f80b6625cc3f4ad639619cc3dee34e675b71c03ef1e0e4370935e87cac5f3c4a72baa6ad61051831d3f2', '54c641a65e5f235fa799e64370c9368ed16559b780ad1d84e5e81aa7cf91e8a45ac3b13dd7538f8f1aa423aeb506c1220e3066c81cd9320781b17921a90902a7', '#ta 118/3, Badda Link road', '01782882820', '', NULL, 'photo.png', 'photo.png', 2, NULL, 1, '2018-04-22', '', 0),
(50, '', '', 'ecommerce', NULL, '01962', 'alamin@jjj.com', '4d1eff3fc19757aadadac0573bb103465980514c1fe68007a1b50ce51ff535b9f26eae106bbee0b9f6a1f8211b5a03189fb7bc230c044f9aed40b5411f5bb637', '91776c6b5d3b8df101a45f26607438d55b030646e75c16b1d532ea7939bd7844c6a94695edd59c953ed7ade48241ff5a31b61065b616881f39c9f3aefb700990', 'asd asdasdad', '', '', NULL, 'photo.png', 'photo.png', 1, NULL, 1, '2018-04-22', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sd_home_edit`
--

CREATE TABLE `sd_home_edit` (
  `id` int(11) NOT NULL auto_increment,
  `menu` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `sd_home_edit`
--

INSERT INTO `sd_home_edit` (`id`, `menu`, `position`) VALUES
(1, 41, 1),
(2, 42, 1),
(3, 41, 2),
(4, 38, 3),
(5, 41, 3),
(6, 42, 4),
(7, 41, 4),
(8, 47, 1),
(9, 53, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sd_item_l`
--

CREATE TABLE `sd_item_l` (
  `id` int(11) NOT NULL auto_increment,
  `item_name` varchar(400) character set utf8 NOT NULL,
  `item_code` varchar(100) default NULL,
  `category` int(11) NOT NULL,
  `sub_cat` int(11) default NULL,
  `sub_sub` int(11) default NULL,
  `brand` varchar(100) default NULL,
  `sort_desc` varchar(600) character set utf8 default NULL,
  `details` text character set utf8,
  `unit` varchar(100) default NULL,
  `price` varchar(100) NOT NULL,
  `affiliate_point` varchar(100) default NULL,
  `discount_per` varchar(100) default NULL,
  `discount_price` varchar(100) NOT NULL,
  `img1` varchar(500) default NULL,
  `img2` varchar(500) default NULL,
  `featured` int(11) NOT NULL default '0',
  `top_pro` int(11) NOT NULL,
  `date` varchar(100) NOT NULL,
  `up_date` varchar(100) NOT NULL,
  `activity` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=300 ;

--
-- Dumping data for table `sd_item_l`
--

INSERT INTO `sd_item_l` (`id`, `item_name`, `item_code`, `category`, `sub_cat`, `sub_sub`, `brand`, `sort_desc`, `details`, `unit`, `price`, `affiliate_point`, `discount_per`, `discount_price`, `img1`, `img2`, `featured`, `top_pro`, `date`, `up_date`, `activity`) VALUES
(274, 'Le Reve Women’s Koti (LSK140', '', 41, 0, NULL, '', NULL, '', '', '1559', NULL, NULL, '1200', 'thumb3-lsk14009-1517472996.jpg', 'thumb2-lsk14009-1517472996.jpg', 0, 0, '2018-02-01', '', 1),
(275, 'Le Reve Semi Fit Women’s Salo', '', 41, 1, 0, '', '', '', '', '1350', NULL, '3', '1100', 'thumb3-skd14648_1-1517473066.jpg', 'thumb2-skd14648_1-1517473066.jpg', 0, 1, '2018-02-01', '', 1),
(277, 'Gentle Park Men&#39;s Blue Check', '', 42, 16, NULL, '', NULL, '', '', '1220', NULL, NULL, '960', 'thumb3-0100322123-1-1517478815.jpg', 'thumb2-0100322123-1-1517478815.jpg', 0, 0, '2018-02-01', '', 1),
(278, 'Pink Cats Maroon And Golden Georgette Saree', '', 41, 2, 0, '', '', '', '', '1022', NULL, '5', '1000', 'thumb3-1-1521607874.jpg', 'thumb2-1-1521607874.jpg', 1, 1, '2018-03-20', '', 1),
(279, ' Outfit Zone Black and White Double Georgette Tops', '', 41, 0, 0, '', '', '', '', '1540', NULL, '0', '1200', 'thumb3-1_(1)-1521608078.jpg', 'thumb2-1_(1)-1521608078.jpg', 1, 1, '2018-03-20', '', 1),
(280, 'Luxe Black Georgette', '', 41, 4, 0, '', '', '', '', '750', NULL, '0', '500', 'thumb3-1_(2)-1521608200.jpg', 'thumb2-1_(2)-1521608200.jpg', 1, 1, '2018-03-20', '', 1),
(281, ' Laksba Pink Cotton T-Shirt', '', 41, 4, 0, '', '', '', '', '650', NULL, '0', '350', 'thumb3-1_(3)-1521608293.jpg', 'thumb2-1_(3)-1521608293.jpg', 0, 1, '2018-03-20', '', 1),
(282, ' Apara White Cotton Heart Beat T-shirt', '', 42, 0, NULL, '', '', '', '', '780', NULL, '0', '500', 'thumb3-1_(4)-1521610786.jpg', 'thumb2-1_(4)-1521610786.jpg', 0, 0, '2018-03-20', '', 1),
(283, 'Apara Gray Cotton Short Sleeve Lucky 7 T-shir', '', 42, 0, NULL, '', '', '', '', '980', NULL, '0', '0', 'thumb3-1_(5)-1521610859.jpg', 'thumb2-1_(5)-1521610859.jpg', 1, 0, '2018-03-20', '', 1),
(284, 'Elegance Park Purple Cotton Long Sleeve Printed Shirt', '', 42, 0, NULL, '', '', '', '', '7856', NULL, '0', '5000', 'thumb3-1_(6)-1521610989.jpg', 'thumb2-1_(6)-1521610989.jpg', 1, 0, '2018-03-20', '', 1),
(285, 'BLU Red Printed Cotton Casual Long Sleeve Shirt', '', 42, 0, NULL, '', '', '', '', '1520', NULL, '0', '1200', 'thumb3-1_(7)-1521611062.jpg', 'thumb2-1_(7)-1521611062.jpg', 0, 0, '2018-03-20', '', 1),
(286, ' Cinderella White Indian Kajol Frock ', '', 47, 0, NULL, '', '', '', '', '546', NULL, '0', '325', 'thumb3-1_(8)-1521611284.jpg', 'thumb2-1_(8)-1521611284.jpg', 1, 0, '2018-03-20', '', 1),
(287, 'Local Shop Multi Color Polyester Backpack', '', 47, 0, NULL, '', '', '', '', '564', NULL, '0', '500', 'thumb3-1_(9)-1521611399.jpg', 'thumb2-1_(9)-1521611399.jpg', 0, 0, '2018-03-20', '', 1),
(288, ' Aalaadin Light Turquoise Cotton T-Shirt and White Pant', '', 47, 0, NULL, '', '', '', '', '789', NULL, '0', '350', 'thumb3-1_(10)-1521611475.jpg', 'thumb2-1_(10)-1521611475.jpg', 0, 0, '2018-03-20', '', 1),
(289, ' KANON Light Gray T-Shirt', '', 47, 0, NULL, '', '', '', '', '630', NULL, '0', '230', 'thumb3-1_(11)-1521611520.jpg', 'thumb2-1_(11)-1521611520.jpg', 0, 0, '2018-03-20', '', 1),
(290, 'Fashion Express Dark Gray Cotton T-Shirt', '', 47, 0, NULL, '', '', '', '', '864', NULL, '0', '630', 'thumb3-1_(12)-1521611562.jpg', 'thumb2-1_(12)-1521611562.jpg', 0, 0, '2018-03-20', '', 1),
(291, 'Xiaomi Mi V2i - Power Bank - 10000mAh', '', 43, 0, NULL, '', '', '', '', '1000', NULL, '3', '780', 'thumb3-1_(13)-1521611785.jpg', 'thumb2-1_(13)-1521611785.jpg', 1, 0, '2018-03-20', '', 1),
(292, 'REMAX PRODA E5 5000mAh Power Bank', '', 43, 0, NULL, '', '', '', '', '864', NULL, '0', '350', 'thumb3-1_(14)-1521611878.jpg', 'thumb2-1_(14)-1521611878.jpg', 0, 0, '2018-03-20', '', 1),
(293, 'REMAX Mini Power Bank 2600mAh ', '', 43, 0, NULL, '', '', '', '', '350', NULL, '0', '250', 'thumb3-1_(15)-1521611920.jpg', 'thumb2-1_(15)-1521611920.jpg', 0, 0, '2018-03-20', '', 1),
(294, ' Gadget Gallery Q7S Smartwatch Phone - Black', '', 43, 0, NULL, '', '', '', '', '2000', NULL, '0', '1500', 'thumb3-1_(16)-1521611974.jpg', 'thumb2-1_(16)-1521611974.jpg', 0, 0, '2018-03-20', '', 1),
(295, ' R3 Smart Band Fitness Tracker Wristband - Black', '', 43, 0, NULL, '', '', '', '', '2500', NULL, '0', '2200', 'thumb3-1_(17)-1521612017.jpg', 'thumb2-1_(17)-1521612017.jpg', 0, 0, '2018-03-20', '', 1),
(296, 'Ayesha Takia Pant Style Pakistani Salwar Kameez For Ceremonial', '', 41, 1, 2, 'SD', 'dfsdfsdf sdf sdfsd\r\n', '<p>dfs dfsdfsdfs dfsd fsdf</p>\r\n', 'PIC', '1650', '100', '0', '0', 'thumb3-ayesha-takia-pant-style-pakistani-salwar-kameez-for-ceremonial-44996-800x1100-1524372771.jpg', 'thumb2-ayesha-takia-pant-style-pakistani-salwar-kameez-for-ceremonial-44996-800x1100-1524372771.jpg', 1, 1, '2018-04-22', '', 1),
(297, 'Grameen Check Purple Cotton Long Sleeve Casual Shirt for Men', '', 53, 6, 3, '', '\r\nWelcome to Grameen Check, the premium fashion and lifestyle brand. We provide fashionable, trendy and quality product at a best price. So be fashionable with Grameen Check.\r\n\r\nKEY FEATURES\r\nProduct Type: Casual Shirt\r\nColor: Purple\r\nMain Material: Cotton\r\nGender: Men\r\n', '<p><br />\r\nWelcome to Grameen Check, the premium fashion and lifestyle brand. We provide fashionable, trendy and quality product at a best price. So be fashionable with Grameen Check.</p>\r\n\r\n<p>KEY FEATURES<br />\r\nProduct Type: Casual Shirt<br />\r\nColor: Purple<br />\r\nMain Material: Cotton<br />\r\nGender: Men</p>\r\n', 'PIC', '697', '50', '0', '0', 'thumb3-1-1524392569.jpg', 'thumb2-1-1524392569.jpg', 0, 1, '2018-04-22', '', 1),
(298, 'Women&#39;s Half Silk Saree', 'MDAY-SHS0350S', 54, 8, 0, 'SADAKALO', 'Shipping From: Bangladesh Dhaka Metropolitan Area:&nbsp;24-48 hours delivery\r\n', '<p><strong>Title:</strong>&nbsp;Women&#39;s Half Silk Saree</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Product Details:</strong></p>\r\n\r\n<p><strong>Type</strong>: Saree</p>\r\n\r\n<p><strong>Gender</strong>: Women&#39;s</p>\r\n\r\n<p><strong>Fabric</strong>: Half Silk</p>\r\n\r\n<p><strong>Design</strong>: Screenprint</p>\r\n\r\n<p><strong>Color</strong>: As shown in the image</p>\r\n', '', '2120', '0', '10', '1850', 'thumb3-0089506_womens-half-silk-saree-bk-01-1526108398.jpeg', 'thumb2-0089506_womens-half-silk-saree-bk-01-1526108398.jpeg', 0, 0, '2018-05-12', '', 1),
(299, 'Ayesha Takia Pant Style Pakistani Salwar Kameez For Ceremonial', '', 42, 0, NULL, '', 'dsfs\r\n', '<p>sdf</p>\r\n', '', '250', '50', '0', '0', 'thumb3-1-1526795787.jpg', 'thumb2-1-1526795787.jpg', 0, 0, '2018-05-20', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sd_more_img`
--

CREATE TABLE `sd_more_img` (
  `id` int(11) NOT NULL auto_increment,
  `pro_id` int(11) NOT NULL,
  `img1` varchar(400) NOT NULL,
  `img2` varchar(400) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sd_more_img`
--

INSERT INTO `sd_more_img` (`id`, `pro_id`, `img1`, `img2`) VALUES
(1, 0, 'thumb3-fastrack-0628-13295-1-catalog_grid_3-1475729954.jpg', 'thumb2-fastrack-0628-13295-1-catalog_grid_3-1475729954.jpg'),
(4, 48, 'thumb3-thumb2-1_(1)-1475159441-1475730622.jpg', 'thumb2-thumb2-1_(1)-1475159441-1475730622.jpg'),
(5, 48, 'thumb3-thumb3-1_(2)-1475159535-1475730662.jpg', 'thumb2-thumb3-1_(2)-1475159535-1475730662.jpg'),
(6, 268, 'thumb3-1_(8)-1517377682.jpg', 'thumb2-1_(8)-1517377682.jpg'),
(7, 268, 'thumb3-3_(1)-1517377693.jpg', 'thumb2-3_(1)-1517377693.jpg'),
(8, 268, 'thumb3-1_(10)-1517377700.jpg', 'thumb2-1_(10)-1517377700.jpg'),
(9, 276, 'thumb3-41b-gp2yajl._sl500_ac_ss350_-1518844782.jpg', 'thumb2-41b-gp2yajl._sl500_ac_ss350_-1518844782.jpg'),
(10, 295, 'thumb3-jeans_png5747-1524206721.png', 'thumb2-jeans_png5747-1524206721.png'),
(11, 295, 'thumb3-jeans_png5775-1524206725.png', 'thumb2-jeans_png5775-1524206725.png'),
(12, 296, 'thumb3-1843sl04-40004-1524372836.jpg', 'thumb2-1843sl04-40004-1524372836.jpg'),
(13, 297, 'thumb3-3-1524392713.jpg', 'thumb2-3-1524392713.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sd_order_list`
--

CREATE TABLE `sd_order_list` (
  `id` int(11) NOT NULL auto_increment,
  `orderID` varchar(100) NOT NULL,
  `pro_id` int(11) NOT NULL,
  `title` varchar(400) NOT NULL,
  `item_code` varchar(200) default NULL,
  `img` varchar(400) NOT NULL,
  `color` varchar(100) default NULL,
  `size` varchar(100) default NULL,
  `price` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `unit` varchar(100) default NULL,
  `line_total` varchar(100) NOT NULL,
  `date` varchar(60) NOT NULL,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `sd_order_list`
--

INSERT INTO `sd_order_list` (`id`, `orderID`, `pro_id`, `title`, `item_code`, `img`, `color`, `size`, `price`, `qty`, `unit`, `line_total`, `date`, `time`) VALUES
(1, 'wr2HjKQnndCrYEk', 0, 'Luxe Black Georgette', '', 'thumb3-1_(2)-1521608200.jpg', NULL, NULL, '500"', 1, '', '500', '2018-03-21', '2018-03-21 02:44:32'),
(2, 'wr2HjKQnndCrYEk', 0, 'BLU Red Printed Cotton Casual Long Sleeve Shirt', '', 'thumb3-1_(7)-1521611062.jpg', NULL, NULL, '1200"', 1, '', '1200', '2018-03-21', '2018-03-21 02:44:32'),
(3, 'GKGHsdaUPmZPB6I', 0, ' Laksba Pink Cotton T-Shirt', '', 'thumb3-1_(3)-1521608293.jpg', NULL, NULL, '350', 1, '', '350', '2018-03-21', '2018-03-21 03:49:49'),
(4, 'DFMHNTY94w5RXEl', 0, ' KANON Light Gray T-Shirt', '', 'thumb3-1_(11)-1521611520.jpg', NULL, NULL, '230', 1, '', '230', '2018-03-22', '2018-03-22 02:53:08'),
(5, 'ng370cAxrDoe12A', 0, 'Elegance Park Purple Cotton Long Sleeve Printed Shirt', '', 'thumb3-1_(6)-1521610989.jpg', NULL, NULL, '5000', 1, '', '5000', '2018-03-24', '2018-03-23 21:01:21'),
(6, 'ozB5iAyaagooIXz', 0, 'Le Reve Semi Fit Women’s Salo', '', 'thumb3-skd14648_1-1517473066.jpg', NULL, NULL, '1100', 1, '', '1100', '2018-04-02', '2018-04-02 13:34:40'),
(7, 'ozB5iAyaagooIXz', 0, ' Outfit Zone Black and White Double Georgette Tops', '', 'thumb3-1_(1)-1521608078.jpg', NULL, NULL, '1200"', 1, '', '1200', '2018-04-02', '2018-04-02 13:34:40'),
(8, 'x7zhV33GuEzpi8X', 0, 'Pink Cats Maroon And Golden Georgette Saree', '', 'thumb3-1-1521607874.jpg', NULL, NULL, '1000"', 1, '', '1000', '2018-04-02', '2018-04-02 14:28:31'),
(9, 'x7zhV33GuEzpi8X', 0, 'Elegance Park Purple Cotton Long Sleeve Printed Shirt', '', 'thumb3-1_(6)-1521610989.jpg', NULL, NULL, '5000"', 1, '', '5000', '2018-04-02', '2018-04-02 14:28:31'),
(10, 'x7zhV33GuEzpi8X', 0, 'Le Reve Semi Fit Women’s Salo', '', 'thumb3-skd14648_1-1517473066.jpg', NULL, NULL, '1100"', 1, '', '1100', '2018-04-02', '2018-04-02 14:28:31'),
(11, '91uIU8d6YOcw6nT', 0, ' Outfit Zone Black and White Double Georgette Tops', '', 'thumb3-1_(1)-1521608078.jpg', NULL, NULL, '1200', 1, '', '1200', '2018-04-02', '2018-04-02 14:39:39'),
(12, '91uIU8d6YOcw6nT', 0, 'Elegance Park Purple Cotton Long Sleeve Printed Shirt', '', 'thumb3-1_(6)-1521610989.jpg', NULL, NULL, '5000', 1, '', '5000', '2018-04-02', '2018-04-02 14:39:39'),
(13, 'S26ig0BAEU55ndD', 0, ' Outfit Zone Black and White Double Georgette Tops', '', 'thumb3-1_(1)-1521608078.jpg', NULL, NULL, '1200', 1, '', '1200', '2018-04-02', '2018-04-02 16:44:33'),
(14, 'S26ig0BAEU55ndD', 0, 'BLU Red Printed Cotton Casual Long Sleeve Shirt', '', 'thumb3-1_(7)-1521611062.jpg', NULL, NULL, '1200', 1, '', '1200', '2018-04-02', '2018-04-02 16:44:33'),
(15, 'S26ig0BAEU55ndD', 0, ' KANON Light Gray T-Shirt', '', 'thumb3-1_(11)-1521611520.jpg', NULL, NULL, '230', 3, '', '690', '2018-04-02', '2018-04-02 16:44:33'),
(16, 'njYgknlZKsTOsye', 0, 'Luxe Black Georgette', '', 'thumb3-1_(2)-1521608200.jpg', NULL, NULL, '500', 1, '', '500', '2018-04-02', '2018-04-02 19:22:53'),
(17, 'njYgknlZKsTOsye', 0, 'Elegance Park Purple Cotton Long Sleeve Printed Shirt', '', 'thumb3-1_(6)-1521610989.jpg', NULL, NULL, '5000', 1, '', '5000', '2018-04-02', '2018-04-02 19:22:53'),
(18, '0DAxJn397rdb7pO', 0, ' Outfit Zone Black and White Double Georgette Tops', '', 'thumb3-1_(1)-1521608078.jpg', NULL, NULL, '1200', 1, '', '1200', '2018-04-20', '2018-04-20 12:21:07'),
(19, 'cHZTkwpCvpJ4OxN', 0, 'Ayesha Takia Pant Style Pakistani Salwar Kameez For Ceremonial', '', 'thumb3-ayesha-takia-pant-style-pakistani-salwar-kameez-for-ceremonial-44996-800x1100-1524372771.jpg', NULL, NULL, '1650', 1, 'PIC', '1650', '2018-04-22', '2018-04-22 11:19:03'),
(20, 'tpmMdWvSmRsjCtG', 0, 'Grameen Check Purple Cotton Long Sleeve Casual Shirt for Men', '', 'thumb3-1-1524392569.jpg', NULL, NULL, '697', 1, 'PIC', '697', '2018-04-22', '2018-04-22 16:26:29'),
(21, 'tpmMdWvSmRsjCtG', 0, 'Ayesha Takia Pant Style Pakistani Salwar Kameez For Ceremonial', '', 'thumb3-ayesha-takia-pant-style-pakistani-salwar-kameez-for-ceremonial-44996-800x1100-1524372771.jpg', NULL, NULL, '1650', 1, 'PIC', '1650', '2018-04-22', '2018-04-22 16:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `sd_order_more`
--

CREATE TABLE `sd_order_more` (
  `id` int(11) NOT NULL auto_increment,
  `order_id` varchar(100) NOT NULL,
  `customerID` varchar(50) default NULL,
  `customer_name` varchar(100) default NULL,
  `mobile` varchar(100) default NULL,
  `extra_number` varchar(200) default NULL,
  `address` varchar(100) default NULL,
  `billing_address` text,
  `g_total` int(11) NOT NULL,
  `discount` varchar(20) default '0',
  `mathod` varchar(100) character set utf8 NOT NULL,
  `shipping` varchar(20) default '0',
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `date` varchar(60) NOT NULL,
  `full_date` varchar(100) NOT NULL,
  `l_time` varchar(100) NOT NULL,
  `activity` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `sd_order_more`
--

INSERT INTO `sd_order_more` (`id`, `order_id`, `customerID`, `customer_name`, `mobile`, `extra_number`, `address`, `billing_address`, `g_total`, `discount`, `mathod`, `shipping`, `time`, `date`, `full_date`, `l_time`, `activity`) VALUES
(1, 'wr2HjKQnndCrYEk', '43', '', '', NULL, 'Dhaka', NULL, 1700, '0', '', '', '2018-03-21 02:44:32', '2018-03-21', '', '15:44:24', 1),
(2, 'GKGHsdaUPmZPB6I', '43', '', '', NULL, 'adorsonogor\r\nlink road', NULL, 350, '0', '', '', '2018-03-21 03:49:49', '2018-03-21', '', '16:49:41', 1),
(3, 'DFMHNTY94w5RXEl', '', '', '', NULL, 'adorsonogor\r\nlink road', NULL, 230, '0', '', '', '2018-03-22 02:53:08', '2018-03-22', '', '15:52:58', 1),
(4, 'ng370cAxrDoe12A', '43', '', '', NULL, 'adorsonogor\r\nlink road', NULL, 5000, '0', '', '', '2018-03-23 21:01:21', '2018-03-24', '', '10:01:17', 1),
(5, 'ozB5iAyaagooIXz', '', 'test employ', '01782882820', NULL, '#Ta 118/3, Gulshan badda link road, badda', NULL, 2300, '0', 'bKash', '100', '2018-04-02 13:34:40', '2018-04-02', '', '13:34:14', 1),
(6, 'x7zhV33GuEzpi8X', '', 'Tapos Chandra Das', '01918163449', NULL, 'fdsdf sdfsds', NULL, 7100, '0', 'bKash', '20', '2018-04-02 14:28:31', '2018-04-02', '', '14:28:14', 1),
(7, '91uIU8d6YOcw6nT', '44', 'shamim', '01918163449', NULL, 'adasd asd asdada', NULL, 6200, '0', 'bKash', '20', '2018-04-02 14:39:39', '2018-04-02', '', '14:39:36', 1),
(8, 'S26ig0BAEU55ndD', '46', 'shamim', '01820762264', NULL, 'sdfsdf', NULL, 3090, '0', 'bKash', '20', '2018-04-02 16:44:33', '2018-04-02', '', '16:44:30', 1),
(9, 'njYgknlZKsTOsye', '46', 'shamim Hossaain', '01820762264', NULL, 'Badda', NULL, 5500, '0', 'bKash', '20', '2018-04-02 19:22:53', '2018-04-02', '', '19:22:50', 2),
(10, '0DAxJn397rdb7pO', '', 'noman ahmed', '01525452152', '01918163449', '#ta 118/3, badda', 'Same As Address', 1200, '0', 'bKash', '100', '2018-04-20 12:21:07', '2018-04-20', '', '12:20:57', 1),
(11, 'cHZTkwpCvpJ4OxN', '49', 'test2', '01673842613', '', '#ta 118/3, Badda Link road', '#ta 118/3, Badda Link road', 1650, '0', 'bKash', '100', '2018-04-22 11:19:03', '2018-04-22', '', '11:19:00', 2),
(12, 'tpmMdWvSmRsjCtG', '48', 'affilate user', '01782882820', '', 'sdfsdf', 'sdfsdf', 2347, '0', 'bKash', '100', '2018-04-22 16:26:30', '2018-04-22', '', '16:26:21', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sd_page_setup`
--

CREATE TABLE `sd_page_setup` (
  `id` int(11) NOT NULL auto_increment,
  `title` text NOT NULL,
  `copyright` text character set latin1 collate latin1_general_cs NOT NULL,
  `top_no` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sd_page_setup`
--

INSERT INTO `sd_page_setup` (`id`, `title`, `copyright`, `top_no`, `email`) VALUES
(1, 'KR Ltd.', 'Copyright 2017-18 All rights reserved dahuwa.com.bd', '+88 0123456789', 'info@dahuwa.com.bd');

-- --------------------------------------------------------

--
-- Table structure for table `sd_payments`
--

CREATE TABLE `sd_payments` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) character set utf8 NOT NULL,
  `value` varchar(100) NOT NULL,
  `position` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sd_payments`
--

INSERT INTO `sd_payments` (`id`, `name`, `value`, `position`) VALUES
(6, 'bKash', '100', '2'),
(8, 'CASH ON DELIVERY (COD)', '60', '1'),
(9, 'Courier Delivery (S.A Paribahan, Sundarban Courier Service)', '100', '2'),
(10, 'bKash', '20', '1');

-- --------------------------------------------------------

--
-- Table structure for table `sd_payout`
--

CREATE TABLE `sd_payout` (
  `id` int(11) NOT NULL auto_increment,
  `send_from` varchar(100) NOT NULL,
  `mobile_to` varchar(100) NOT NULL,
  `transfer_to` varchar(100) NOT NULL,
  `mathod` varchar(100) NOT NULL,
  `mbl_number` varchar(100) default NULL,
  `bank_name` varchar(200) default NULL,
  `branch` varchar(100) default NULL,
  `name` varchar(100) NOT NULL,
  `ac_number` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `sc_code` varchar(100) NOT NULL,
  `session` varchar(200) default NULL,
  `op_type` varchar(200) NOT NULL,
  `prev_due` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `c_due` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `activity` int(11) NOT NULL default '2',
  `data_type` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `sd_payout`
--

INSERT INTO `sd_payout` (`id`, `send_from`, `mobile_to`, `transfer_to`, `mathod`, `mbl_number`, `bank_name`, `branch`, `name`, `ac_number`, `type`, `sc_code`, `session`, `op_type`, `prev_due`, `amount`, `c_due`, `date`, `activity`, `data_type`) VALUES
(34, '01782882820', '', '', 'Mobile Recharge', '01782882820', NULL, NULL, '', '', '', '', NULL, '', '2000', '500', '1500', '2018-04-20', 1, 1),
(35, '48', '', '', 'Mobile Recharge', '01680762264', NULL, NULL, '', '', '', '', NULL, '', '2000', '700', '1300', '2018-04-20', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sd_point_count`
--

CREATE TABLE `sd_point_count` (
  `id` int(11) NOT NULL auto_increment,
  `in_id` varchar(100) default NULL,
  `customerID` varchar(100) default NULL,
  `total_order` varchar(100) NOT NULL,
  `amount` varchar(200) NOT NULL,
  `date` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `in_id` (`in_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sd_point_count`
--

INSERT INTO `sd_point_count` (`id`, `in_id`, `customerID`, `total_order`, `amount`, `date`) VALUES
(1, 'njYgknlZKsTOsye', '46', '5500', '550', '2018-04-02 19:28:32'),
(3, 'cHZTkwpCvpJ4OxN', '01782882820', '1650', '165', '2018-04-22 11:25:13');

-- --------------------------------------------------------

--
-- Table structure for table `sd_posts`
--

CREATE TABLE `sd_posts` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(400) character set utf8 NOT NULL,
  `sort_desc` varchar(500) character set utf8 default NULL,
  `full_desc` text character set utf8 NOT NULL,
  `img1` varchar(500) default NULL,
  `img2` varchar(500) default NULL,
  `type` int(11) NOT NULL default '1',
  `date` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `sd_posts`
--

INSERT INTO `sd_posts` (`id`, `title`, `sort_desc`, `full_desc`, `img1`, `img2`, `type`, `date`) VALUES
(1, 'About US', '<p>About Us<br></p>', '<p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p><br></p><p>The nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs&nbsp;<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n</p>\r\n\r\n<p><br></p><p>\r\n\r\n</p><p>The nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n<br></p><p>\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n\r\n\r\n\r\nThe nation is paying glowing tributes to the Language Movement martyrs\r\n</p>\r\n\r\n<br><p></p>', 'thumb3-alka-yagnik-latest-images-1467400887.jpg', 'thumb2-alka-yagnik-latest-images-1467400887.jpg', 2, '2016-07-01'),
(2, 'FAQ', '<p>e5656</p>', '<h4>How To Order?</h4><p>sdgsergsert tg hertghrthg erherty ertherhyeey &nbsp; h e&nbsp;<b></b></p><p>\r\n\r\n</p><h4>How To Order?</h4><p>sdgsergsert tg hertghrthg erherty ertherhyeey &nbsp; h e </p>\r\n\r\n<br><p></p><p><br></p>', NULL, NULL, 2, '2016-07-08'),
(3, 'Contact Us', NULL, '<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14602.705266363106!2d90.424671!3d23.794538!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x6a7d899571ae8c2f!2sStar+Design+BD!5e0!3m2!1sen!2sbd!4v1475736634495" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>\r\n\r\n<h3>Contact Us:</h3>\r\n\r\n<p><strong>Address: </strong>Ga# 36, Sahajazpur, notun Bazar, Badda, Dhaka.<br />\r\n<strong>Call: </strong>01782882820, 01673842613.</p>\r\n', NULL, NULL, 2, '2016-07-08'),
(5, 'Order And Payment', NULL, '<p><strong>Normal Delivery:</strong>&nbsp;If your shipping address is within Dhaka city, products will be delivered by next business day. If it is outside Dhaka then it will take 1-3 business days.<br />\r\ncall for support: 017-88888 952, 017-88888 953</p>\r\n\r\n<p><br />\r\n<strong>Delivery Services:</strong>&nbsp;We provide Cash On Delivery (COD) service all over Bangladesh.Customers need to pay only after receiving the product. Customers outside of Dhaka city need to pay only the courier charge (100BDT) in advance in order to confirm their order placement. We deliver our products via SA Paribaan Courier Service or Sundarban Courier Service.<br />\r\nFor Delivery by SA Paribahan, you will have to pay &amp; collect your product from your nearest SA Paribahan office hand to hand.</p>\r\n\r\n<p>For Delivery by Sundarban Courier Service, you will have to make full payment in advance by bKash, UKash, DBBL mobile money transfer. You will get your product in your home/office address. So you won&#39;t have to go any other place to collect your parcel.</p>\r\n\r\n<p><br />\r\n<strong>Available Payment Mathods:</strong>&nbsp;We accept -</p>\r\n\r\n<p>*bKash</p>\r\n\r\n<p>*UKash</p>\r\n\r\n<p>*DBBL</p>\r\n', NULL, NULL, 2, ''),
(7, 'Cancellation & Returns', NULL, '<p><strong>Products Return policy</strong></p><p>We provide 3 days return policy for unused, with original packaging product. We wil change the product if it is defected or not the same product as you ordered or as we described. For the products that qualifies for free delivery it is our strong belief that you shouldn''t consume something which you don''t enjoy and we will do anything to make sure you enjoy the product you have ordered. If a suitable replacement is not possible then we will refund 90% of the price you paid.</p><p><strong>Cancellation of order</strong></p><p><br>If any customer wants to cancel their order, they have to cancel it within 6 hours of order placement.</p><p>As we try our best to ship the product as soon as we can, so if we already shipped the product and you still wants to cancel then you will only have to pay one way delivery charge.<br><br>For Dhaka Metro: 50 BDT</p><p>Others: 100 BDT</p><p><br></p><p>anything.com.bd reserve all rights to change any policy without prior notice.</p>', NULL, NULL, 2, ''),
(8, 'Condition to Terms ', NULL, '<p>Anything.com.bd provides its services to you subject to the notices, terms and conditions set forth in this agreement (the &quot;Agreement&quot;). If you visit us at Anything.com.bd, you accept these Terms and Conditions. Please read them carefully. In addition, when you use any Anything.com.bd service (e.g., Customer Reviews), you will be subject to the rules, guidelines, policies, terms, and conditions applicable to such service, and they are incorporated into this Agreement by this reference. Lalfita.com reserves the right to change this Site and these terms and conditions at any time. Anything.com.bd retains the right to deny access to anyone who we believe has violated any of these Terms of Use.<br />\r\n<br />\r\n<strong>Electronic Communication </strong><br />\r\nWhen you visit Anything.com.bd or send e-mails to us or give us your feedback, you are communicating with us electronically. You consent to receive communications from us electronically. We will communicate with you by e-mail or by posting notices on this website or by phone. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy any legal requirement that such communications be in writing.<br />\r\n<br />\r\n<strong>Contents Posted on Site</strong><br />\r\nYou are responsible for any notes, messages, e-mails, billboard postings, photos, drawings, profiles, opinions, ideas, images, videos, audio files or other materials or information posted or transmitted to the Sites (collectively, &quot;Content&quot;). Such Content will become the property of Lalfita.com throughout the universe. Lalfita.com reserves the right (but not the obligation) to remove or edit such content, but does not regularly review posted content. You agree that any Content you post may be used by Anything.com.bd, consistent with our Privacy Policy and Rules of Conduct on Site as mentioned herein, and you are not entitled to any payment or other compensation for such use.<br />\r\n<br />\r\n<strong>Links</strong><br />\r\nThis site may contain links to other web sites or you may be linked through other web sites (&quot;Linked Sites&quot;). The Linked Sites are for your convenience only and you access them at your own risk. We are not responsible for the content of the Linked Sites, whether or not Anything.com.bd is affiliated with sponsors of the sites.<br />\r\n<br />\r\nWe do not in any way endorse the Linked Sites. We welcome links to this site. You may establish a hypertext link to this site, provided that the link does not state or imply any sponsorship or endorsement of your site by Lalfita.com. You must not use on your site or in any other manner any trademarks, service marks or any other materials appearing on the Lalfita.com site, including any logos or characters, without the express written consent of the owner of the mark or materials. You must not frame or otherwise incorporate into another web site or present in conjunction with or juxtaposed against such a web site any of the content or other materials on the Lalfita.com site without our prior written consent.<br />\r\n<br />\r\n<strong>Disclaimer of warranties and limitation of liability</strong><br />\r\nLalfita.com has made this service available to use as a matter of convenience. Anything.com.bd expressly disclaims any claim or liability arising out of uploading of any obscene, vulgar or pornographic images, photograph or a picture or altering or distorting the images available under this service in an obscene, vulgar or pornographic manner. Anything.com.bd also disclaims all warranties, express or implied, including, but not limited to, implied warranties of merchantability and fitness for a particular purpose. Anything.com.bd does not warrant that this site, its servers, or e-mail sent from Anything.com.bd are free of viruses or other harmful components. Anything.com.bd will not be liable for any damages of any kind arising from the use of this site, including, but not limited to direct, indirect, incidental, punitive, and consequential damages.<br />\r\n<br />\r\nThe Service(s) of Anything.com.bd is provided on an &quot;as is&quot; basis without warranties of any kind, whether express or implied. Anything.com.bd does not represent or warrant maintaining the confidentiality of information; although Anything.com.bd&#39;s current practice is to ensure reasonable efforts to maintain such confidentiality. It is also clearly understood by the User that all warranties and after sales services, implied or express, take place directly between the vendors and the User/buyer/customer and the terms of sale by the vendor<br />\r\n<br />\r\nAnything.com.bd does not endorse in anyway any advertisers/ contents of advertisers on its web pages or other communication.<br />\r\n<br />\r\nAnything.com.bd will not be responsible for any damage suffered by Users from use of the services on this site. This without limitation includes loss of revenue/data resulting from delays, non-deliveries, missed deliveries, or service interruptions as may occur because of any act / omission of the vendor. This disclaimer of liability also applies to any damages or injury caused by any failure of performance, error, omission, interruption, deletion, defect, delay in operation or transmission, computer virus, communication line failure, theft or destruction or unauthorized access to, alteration of, or use of record, whether for breach of contract, tortuous behavior, negligence, or under any other cause of action.<br />\r\n<br />\r\nUser agrees and acknowledges that User shall be solely responsible for User&#39;s conduct and that Lalfita.com reserves the right to terminate your rights to use the service immediately, notwithstanding penal provisions under the Bangladesh cyber laws or any other allied laws enacted by the government of Bangladesh or any other statutory, legislative or regulatory authority authorized in this regard from time to time. In no event shall Lalfita.com, its affiliates, employees, agents, consultants, contracted companies be liable for any direct, indirect, punitive, incidental, special or consequential damages or for any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the use or performance of the Lalfita.com sites/services for interrupted communications, delay, lost data or lost profits arising out of or in connection with this agreement.<br />\r\n<br />\r\nAnything.com.bd therefore neither endorses nor offers any judgment or warranty and accepts no responsibility or liability for the authenticity/availability of any of the goods/services/or for any damage, loss or harm, direct or consequential or any violation of local or international laws that may be incurred by your visit and/or transaction/s on these sites.<br />\r\n<br />\r\nAnything.com.bd shall not be liable for any delay / non-delivery of purchased goods by the vendors, trade organization/s, manufacturers / shop etc. (vendors), due to flood, fire, wars, acts of God or any cause that is beyond the control of Anything.com.bd.<br />\r\n<br />\r\nNotwithstanding anything contained herein, Anything.com.bd acts only as a booking agent/platform to facilitate / integrate transactions between the Users/buyers and sellers through various platforms (online web store, call centre, mail order catalogues, SMS, etc.) for various vendors/affiliates/merchants and shall in no way be responsible for any quality of product, damages, losses, expenses and/or taxes incurred by Users for the products or if the vendors/affiliates/merchants is not able to service the order of the User for any reason or any misrepresentation of any sort by the vendors/affiliates/merchants. In no event shall Anything.com.bd, its directors, officials, representatives and employees be liable for any damages or claims relating to products sold through its various platforms.<br />\r\n<br />\r\nAll prices, unless indicated otherwise are in Bangladeshi Taka. The availability of products is subject to change without prior notice at the sole discretion of Anything.com.bd and orders can be cancelled if the product goes out of stock with the Vendor/Seller. Anything.com.bd reserves the right to refuse or cancel any order placed for a product that is listed at an incorrect price which may be higher or lower than published. This shall be regardless of whether the order has been confirmed and/or payment been levied via credit card or cheque or otherwise. In the event the payment has been processed by Anything.com.bd the refund amount shall be credited to your credit card account and duly notified to you by email or sent by cheque.<br />\r\n<br />\r\nIn a credit/debit card transaction, you must use a credit /debit card that is issued in the name of the User. Anything.com.bd will not be liable for any credit/debit card fraud because of the card being used fraudulently. The liability to use a credit/debit card or a net banking transaction fraudulently will be on the User and the onus to &#39;prove otherwise&#39; shall be exclusively on the User.<br />\r\n<br />\r\nAny request for cancellation of orders once duly placed on the site, shall not be entertained.<br />\r\n<br />\r\nIn the event that a non-delivery occurs on account of a mistake by you (i.e. wrong name or address) any extra cost incurred by Anything.com.bd for re-delivery shall be claimed from the User placing the order.<br />\r\n<br />\r\nAll products are duly screened and assured by the vendors to ensure that the products are of the standard, quality, composition, style or model that they represent and as displayed on Anything.com.bd on behalf of the vendor/merchant/affiliate/merchant. All and any additional information/description, etc. for a product that is displayed/showcased on Anything.com.bd is on behalf of the vendor/merchant/affiliate/manufacturer and is as provided to Anything.com.bd. Anything.com.bd does not take any responsibility for any incorrect or error in the display/showcase of such information.<br />\r\n<br />\r\nPresently, the service(s) of Anything.com.bd Shopping are being offered free. However, Anything.com.bd reserves the right to charge a fee for any or such facilities or freight or handling charges or shipping charges or statutory taxes as per the terms of the buyer/seller. Anything.com.bd reserve the right to cancel orders for the product in their sole discretion for any reason which can include but not limited to, the product being out of stock, or for any other reason without intimation to the User.<br />\r\n<br />\r\nYou will be required to enter a valid phone number while placing an order on Anything.com.bd. By registering your phone number with us, you consent to be contacted by Anything.com.bd via phone calls and / or SMS notifications, in case of any order or shipment or delivery related updates. Anything.com.bd will not use your personal information to initiate any promotional phone calls or SMS&#39;s.<br />\r\n<br />\r\n<strong>Payment</strong><br />\r\nWhile availing any of the payment method/s offered by us, we are not responsible or take no liability of whatsoever nature in respect of any loss or damage arising directly or indirectly to you out of the decline due to:<br />\r\n1. Lack of authorization for any transaction/s,<br />\r\n2. or exceeding the preset limit mutually agreed by you and between your &quot;Bank/s&quot;,<br />\r\n3. or any payment issues arising out of the transaction,<br />\r\n4. or decline of transaction for any other reason/s.<br />\r\n<br />\r\nAll payments made against the purchases/services on Anything.com.bd by you shall be in Bangladeshi Taka and other International currencies.<br />\r\n<br />\r\n<strong>Applicable Law</strong><br />\r\nThis Agreement shall be governed by and interpreted and construed in accordance with the laws of Bangladesh.<br />\r\n<br />\r\n<strong>Trademark, Copyright and Restriction</strong><br />\r\nThis site is controlled and operated by Anything.com.bd All material on this site, including images, illustrations, audio clips, and video clips, are protected by copyrights, trademarks, and other intellectual property rights that are owned and controlled by us or by other parties that have licensed their material to us. Material on Lalfita.com web site owned, operated, licensed or controlled by us is solely for your personal, non-commercial use. You must not copy, reproduce, republish, upload, post, transmit or distribute such material in any way, including by e-mail or other electronic means and whether directly or indirectly and you must not assist any other person to do so. Without the prior written consent of the owner, modification of the materials, use of the materials on any other web site or networked computer environment or use of the materials for any purpose other than personal, non-commercial use is a violation of the copyrights, trademarks and other proprietary rights, and is prohibited. Any use for which you receive any remuneration, whether in money or otherwise, is a commercial use for the purposes of this clause.<br />\r\n<br />\r\n<strong>Your Account</strong><br />\r\nIf you use this site, you are responsible for maintaining the confidentiality of your account and password and for restricting access to your computer, and also you agree to accept responsibility for all activities that occur under your account or password. Anything.com.bd does sell products for children, but it sells them to adults, who can purchase with a credit card or otherwise. If you are under 18, you may use Lalfita.com only with involvement of a parent or guardian or otherwise, Anything.com.bd has the right to cancel any order or service to the User. Anything.com.bd reserves the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion. Anything.com.bd reserves the right to refuse service, terminate accounts, remove or edit content, or cancel orders in their sole discretion<br />\r\n<br />\r\nAnything.com.bd is associated with various business partners for the supply and service of goods directly to the customers. Anything.com.bd is a marketplace for the products with its business partners. The after sales service and warranty for the products sold by our business partners, as duly applicable, for the respective products, will be taken undertaken and handled by the respective business partners or through their respective service centers.<br />\r\n<br />\r\n<strong>Risk of loss</strong><br />\r\nAll items purchased from Anything.com.bd are made pursuant to a shipment contract. This means that the risk of loss and title for such items pass to you upon our delivery to the carrier.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>anything.com.bd reserve all rights to change any policy without prior notice.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', NULL, NULL, 2, ''),
(9, 'About Us (Footer)', NULL, '<p>A group of young enterpreneur founded onlinehat.net&nbsp;at 10 July, 2016. Main goal of Mawna.com is to achieve customers satisfaction &amp; provide them good support. A group of young enterpreneur founded Mawna.com at 10 July, 2016. Main goal of Mawna.com is to achieve customers satisfaction &amp; provide them good support. Our office is located at Mawna.</p>\r\n', NULL, NULL, 1, ''),
(10, 'Privacy policy', 'Privacy policy', 'Privacy policy', NULL, NULL, 2, ''),
(11, 'about home', 'ads', '<p>Daraz.com.bd (previously kaymu bd) is #1 online shopping website in bangladesh offering bundle deals and discount prices for fashion, electronics and household products. Experience fast, reliable and convenient online shopping experience to discover new styles in&nbsp;<a href="https://www.daraz.com.bd/mens-fashion/">mens fashion</a>,&nbsp;<a href="https://www.daraz.com.bd/womens-fashion/">womens fashion</a>&nbsp;and&nbsp;<a href="https://www.daraz.com.bd/fashion-by-daraz-kids/">kids fashion</a>. Find best trends in fashion according to seasons and occasions with daraz online shopping and remain in style 24x7 and 365 days a year. Shop online for cash on delivery on famous brands like Yellow, Noir, Ecstasy and Texmart among many others that tend to attract the fashionista within you. Daraz is recognized among best online shopping sites in bd offering best deals on&nbsp;<a href="https://www.daraz.com.bd/mens-panjabi-sherwani/">mens sherwanis</a>,&nbsp;<a href="https://www.daraz.com.bd/mens-shirts/">mens shirts</a>, t-shirts, coats and&nbsp;<a href="https://www.daraz.com.bd/mens-shoes/">shoes for men</a>&nbsp;etc.</p>\r\n\r\n<p>For shoppers across the country, Daraz bd has become an online bazar in bangladesh for fashion, clothing &amp; accessories for men &amp; women. Everyone is encouraged to shop with confidence at Daraz.com.bd as our strict buyer&rsquo;s protection policies ensure no risks attached offering finest experience of online shopping in bangladesh. Buyers can ensure legitimacy of product with genuine reviews by satisfied consumers. Also, buyers are provided with full price disclosure without involvement of additional fees during checkout procedures. Moreover, Daraz Bangladesh does not deal in counterfeit goods as all products showcased on our website are legitimate and sold by verified vendors in Bd. Lastly, to keep yourself ahead of the fashion curve, you can visit&nbsp;<a href="https://www.daraz.com.bd/blog/">daraz blog</a>&nbsp;to stay update with latest in-fashion trends and new arrivals on different stores on our website.</p>\r\n\r\n<p>Experience best online shopping in bangladesh with payment by cash on delivery for genuine, legitimate and branded products with buyers protection. Look no further for elite line of&nbsp;<a href="https://www.daraz.com.bd/womens-clothing/">womens clothing</a>&nbsp;in both western and traditional styles. From&nbsp;<a href="https://www.daraz.com.bd/womens-saree/">sarees</a>&nbsp;to&nbsp;<a href="https://www.daraz.com.bd/womens-shalwar-kameez/">womens salwar kameez</a>&nbsp;and&nbsp;<a href="https://www.daraz.com.bd/womens-shoes/">womens shoes</a>&nbsp;to&nbsp;<a href="https://www.daraz.com.bd/womens-accessories/">womens accessories</a>, you can find everything under one roof at daraz.com.bd. To buy retail products at wholesale discount prices, do also checkout our premium&nbsp;<a href="https://www.daraz.com.bd/deals-under-999/">deals under 999</a>&nbsp;and latest&nbsp;<a href="https://www.daraz.com.bd/deals-of-the-week/">deals of the week</a>&nbsp;only available at largest online shop in bd.</p>\r\n\r\n<p><em>Avoid visiting crowded shopping malls and grab new deals on Daraz bd sale items and with ultimate experience of online shopping in dhaka, chittagong, rajshahi, khulna, rangpur and barisal with cash on delivery at your home.</em></p>\r\n\r\n<p>Now with Daraz mobile phone app, you can experience best online shopping in bangladesh with home delivery. Either you are iPhone lover or android smartphone geek, you are encouraged to download Daraz app for Android &amp; IOS.</p>\r\n', NULL, NULL, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sd_settings`
--

CREATE TABLE `sd_settings` (
  `id` int(11) NOT NULL auto_increment,
  `company` varchar(400) NOT NULL,
  `branch` varchar(100) NOT NULL,
  `mobile` varchar(100) NOT NULL,
  `email` varchar(100) default NULL,
  `address` text NOT NULL,
  `img` varchar(400) default NULL,
  `activity` int(11) NOT NULL default '1',
  `up_date` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sd_settings`
--

INSERT INTO `sd_settings` (`id`, `company`, `branch`, `mobile`, `email`, `address`, `img`, `activity`, `up_date`) VALUES
(1, 'Tokyohalalshop.com', 'Mitali Electronics', '+81.0808727', 'support@tokyohalalshop.com', 'Street:2-16-11 Diamond Building 4F, Akabane minami kita-ku\r\nCity:Tokyo', 'thumb2-login-logo-1473138912.png', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sd_slide_mng`
--

CREATE TABLE `sd_slide_mng` (
  `id` int(11) NOT NULL auto_increment,
  `title_one` varchar(500) default NULL,
  `title_two` varchar(500) default NULL,
  `img1` varchar(500) NOT NULL,
  `img2` varchar(500) NOT NULL,
  `cat` varchar(50) NOT NULL,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `sd_slide_mng`
--

INSERT INTO `sd_slide_mng` (`id`, `title_one`, `title_two`, `img1`, `img2`, `cat`, `time`) VALUES
(30, '#', NULL, 'thumb3-slide-1521453513.jpg', 'thumb2-slide-1521453513.jpg', '999999989', '2018-03-19 02:58:37'),
(31, '#', NULL, 'thumb3-slide1-1521453528.jpg', 'thumb2-slide1-1521453528.jpg', '999999989', '2018-03-19 02:58:52'),
(33, '#', NULL, 'thumb3-aarong-boishakhi-collection-2017-sharee-1521453659.jpg', 'thumb2-aarong-boishakhi-collection-2017-sharee-1521453659.jpg', '999999989', '2018-03-19 03:01:02'),
(34, '#', NULL, 'thumb3-screen-shot-2013-12-07-at-6.59.02-am-1521453727.png', 'thumb2-screen-shot-2013-12-07-at-6.59.02-am-1521453727.png', '999999989', '2018-03-19 03:02:11'),
(35, '#', NULL, 'thumb3-fashionable-1521611208.jpg', 'thumb2-fashionable-1521611208.jpg', '999999989', '2018-03-20 22:46:52'),
(36, 'http://localhost/ghor-bazar/page_id/53', NULL, 'thumb3-no-mans-sky-banner-1524394107.jpg', 'thumb2-no-mans-sky-banner-1524394107.jpg', '999999989', '2018-04-22 16:48:30');

-- --------------------------------------------------------

--
-- Table structure for table `sd_social`
--

CREATE TABLE `sd_social` (
  `id` int(11) NOT NULL auto_increment,
  `fc` text NOT NULL,
  `twitter` text NOT NULL,
  `google` text NOT NULL,
  `pin` varchar(100) NOT NULL,
  `instagram` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sd_social`
--

INSERT INTO `sd_social` (`id`, `fc`, `twitter`, `google`, `pin`, `instagram`) VALUES
(1, 'https://www.facebook.com/stardesignbd', '#', 'alaminhossain986@gmail.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sd_third_sub`
--

CREATE TABLE `sd_third_sub` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) character set utf8 NOT NULL,
  `sub_menu_id` int(11) NOT NULL,
  `meta_desc` text,
  `meta_key` text,
  `page_cn` text,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `img1` varchar(400) default NULL,
  `img2` varchar(400) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `sd_third_sub`
--

INSERT INTO `sd_third_sub` (`id`, `name`, `sub_menu_id`, `meta_desc`, `meta_key`, `page_cn`, `time`, `img1`, `img2`) VALUES
(1, 'Saree', 1, ' ', ' ', '', '2018-03-19 21:42:07', NULL, NULL),
(2, 'Salwar Kameez', 1, ' ', ' ', '', '2018-03-19 21:42:59', NULL, NULL),
(3, 'Final Thrd Cat', 6, ' ', ' ', '', '2018-04-22 16:19:24', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sd_tsk_lst`
--

CREATE TABLE `sd_tsk_lst` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(20) NOT NULL,
  `sl` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `sd_tsk_lst`
--

INSERT INTO `sd_tsk_lst` (`id`, `title`, `sl`) VALUES
(1, '1', '1'),
(2, '2', '2'),
(3, '3', '3'),
(4, '4', '4'),
(5, '5', '5'),
(6, '6', '6'),
(7, '7', '7'),
(8, '8', '8'),
(9, '9', '9'),
(10, '10', '10');

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `sub_menu_id` int(11) NOT NULL auto_increment,
  `sub_menu` varchar(50) character set utf8 NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(200) NOT NULL,
  `meta_desc` text,
  `meta_key` text,
  `page_cn` text,
  `img1` varchar(400) default NULL,
  `img2` varchar(400) default NULL,
  PRIMARY KEY  (`sub_menu_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `sub_menu`
--

INSERT INTO `sub_menu` (`sub_menu_id`, `sub_menu`, `menu_id`, `menu_name`, `meta_desc`, `meta_key`, `page_cn`, `img1`, `img2`) VALUES
(1, 'Traditional Wear', 41, '', ' ', ' ', '', NULL, NULL),
(2, 'Innerwear & Nightwear', 41, '', ' ', ' ', '', NULL, NULL),
(4, 'Western Clothing', 41, '', ' ', ' ', '', NULL, NULL),
(5, 'Women&#39;s Jewellery', 41, '', ' ', ' ', '', NULL, NULL),
(6, 'Man Sub Cat', 53, '', ' ', ' ', '', NULL, NULL),
(7, 'Man Sub Cat 02', 53, '', ' ', ' ', '', NULL, NULL),
(8, 'Saree', 54, '', ' ', ' ', '', NULL, NULL);
